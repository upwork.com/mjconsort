from threading import Thread

from mjconsort.producer.produce import BinaryLogReader


if __name__ == "__main__":
    thread = Thread(target=BinaryLogReader().process)
    thread.start()
