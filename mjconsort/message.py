from typing import Union
import logging
import time

import msgpack
import redis

from mjconsort.objtypes import BinaryLogEvent
from mjconsort.objtypes import UnknownEvent
from mjconsort import config
from mjconsort import utils

utils.Logger.start()


class WithRedis:
    """Connect to a remote redis server.

    Handles connect, send and recieve transactions. A queue is
    established after a connection is made using default configuration
    located in config.py. Messages (BinaryLogEvents) are placed into
    the queue using RPUSH, retrieved using LPOP.

    NOTE: The default JSON implementation cannot handle binary data.
    MedJacket's schema uses Binary UUID as primary keys on several
    tables, images are also persisted to tables in binary format.

    """

    def __init__(self):
        self.redis = redis.Redis(
            **config.REDIS_SETTINGS['SERVER'],
            retry_on_timeout=True,
            health_check_interval=30
        )

    def connect(self):
        """Check for a valid connection.

        Retry until it succeeds.
        """
        while True:
            try:
                self.redis.client()
            except redis.exceptions.ConnectionError:
                logging.info('Redis at '
                             f'{config.REDIS_SETTINGS["SERVER"]["host"]}, FAIL')
                time.sleep(1)
                logging.info('RETRYING!')
                continue
            break
        logging.info(f'Redis at {config.REDIS_SETTINGS["SERVER"]["host"]}, OK!')
        return self

    def send(self, event: BinaryLogEvent) -> bool:
        """Send an Message.

        Args:
            event: BinaryLogEvent

        Returns:
            True on success, False otherwise
        """
        while True:
            try:
                event_b = msgpack.packb(event.asdict(), use_bin_type=True)
                if self.redis.rpush(config.REDIS_SETTINGS['KEY'], event_b):
                    return True
            except redis.exceptions.ConnectionError:
                logging.info(f'Redis send fail! on {event.table} ..RETRYING !')
                time.sleep(1)
                continue

    def recv(self) -> Union[dict, UnknownEvent]:
        """Retrieve an event.

        Returns:
            dict:
        """
        while True:
            try:
                event_b = self.redis.lpop(config.REDIS_SETTINGS['KEY'])
                return msgpack.unpackb(event_b, use_list=False, raw=False)
            except (TypeError, KeyError):
                return UnknownEvent()
            except redis.exceptions.ConnectionError:
                logging.info('Redis Queue: Failed to retrieve message!')
                time.sleep(1)
                continue
