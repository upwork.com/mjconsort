"""MJCONSORT.CONSUMER

Pulls messages out a redis queue, process them
and execute on a mysql db server. Uses peewee to
execute.
"""
import logging
from typing import Union

import redis
from peewee import BinaryUUIDField, IntegrityError

from mjconsort.db.conn import DB_CONN
from mjconsort.db.user import user
from mjconsort.message import WithRedis
from mjconsort.objtypes import BinaryLogEvent
from mjconsort.objtypes import UnknownEvent
from mjconsort.objtypes import WriteEvent
from mjconsort.objtypes import DeleteEvent
from mjconsort.objtypes import UpdateEvent
from mjconsort.objtypes import TTableProperties
from mjconsort import utils
# from mjconsort.schema.patients import patients
# from mjconsort.schema.devices import devices
# from mjconsort.schema.hospital import hospital
# from mjconsort.schema.usergroups import usergroups
# from mjconsort.schema.user import user
# from mjconsort.schema.patient_family_details import patient_family_details
# from mjconsort.schema.patient_habits import patient_habits
# from mjconsort.schema.patient_insurances import patient_insurances
# from mjconsort.schema.patient_surgeries import patient_surgeries
# from mjconsort.schema.patient_health_cards import patient_health_cards
from mjconsort.db import models

utils.Logger.start()


REDIS = WithRedis()
TABLES = {
    'patients': models.Patients,
    'devices': models.Devices,
    'hospital': models.Hospital,
    'usergroups': models.Usergroups,
    'user': models.User,
    'patient_family_details': models.PatientFamilyDetails,
    'patient_habits': models.PatientHabits,
    'patient_insurances': models.PatientInsurances,
    'patient_surgeries': models.PatientSurgeries,
    'patient_health_cards': models.PatientHealthCards
}


def get_event_from_queue() -> Union[BinaryLogEvent, UnknownEvent]:
    """Retrieve a message from a queue.
    After unpacking with msgpack, a dict is realised. The object
    is rebuilt into a BinaryLogEvent

    Returns:
        BinaryLogEvent
    """
    event_u = REDIS.recv()

    if isinstance(event_u, UnknownEvent):
        logging.info('Waiting for an event!')
        return UnknownEvent

    return BinaryLogEvent(
        schema=event_u['schema'],
        row=utils.parse_row(event_u['row'], event_u['event_type']),
        table=event_u['table'],
        primary_key=event_u['primary_key'],
        timestamp=event_u['timestamp'],
        event_type=utils.event_type(event_u['event_type']),
    )



def replay_event(event: BinaryLogEvent) -> None:
    """Replay the data on the database.

    Identifies table names, primary keys, field types,
    before replaying change data on the database. Delete, Writes
    and Saves data using the primary_key, which is available
    in the BinaryLogEvent.

    Todo:
        Write Fn:table_properties
    """

    if isinstance(event.event_type, WriteEvent):
        logging.info('Found WriteEvent, replaying ...')
        table = TABLES[event.table]
        try:
            with DB_CONN.atomic():
                table.insert(**event.row.values).execute()
                logging.info(f'Saved a {event.table} to the database')
        except IntegrityError as error:
            logging.info(f'We may have encountered a duplicate record {error}')

    if isinstance(event.event_type, UpdateEvent):
        logging.info('Found UpdateEvent, replaying ...')
        table = TTableProperties(TABLES[event.table])
        if table:
            row = event.row.before_values
            row_pk_value = row[table.pk_name]
            event.row.after_values.pop(table.pk_name)
            with DB_CONN.atomic():
                table.table.update(**event.row.after_values).where(table.pk == row_pk_value).execute()
                logging.info(f'UPDATED {event.table}')

    if isinstance(event.event_type, DeleteEvent):
        logging.info('Found DeleteEvent, replaying ...')
        table = TTableProperties(TABLES[event.table])
        if table:
            row = event.row.values
            row_pk_value = row[table.pk_name]
            with DB_CONN.atomic():
                table.table.delete_by_id(row_pk_value)
                logging.info(f'DELETED record from {event.table}')
