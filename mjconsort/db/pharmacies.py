from peewee import CharField

from .conn import BaseModel

class pharmacies(BaseModel):
    uuid = CharField(max_length=36, null=False, unique=True, primary_key=True)
    email = CharField(null=True)
    address = CharField(null=True)
    city = CharField(null=True)
    telephone = CharField(null=True)
    name = CharField(null=True)