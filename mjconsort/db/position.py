from typing import Union, Any

from peewee import Model
from peewee import SqliteDatabase

from peewee import CharField
from peewee import TimestampField
from peewee import IntegerField
from peewee import Check

from mjconsort import config


DB_CONN = SqliteDatabase(
    f'{config.BASE_PATH}/mjconsort/db/db.sqlite',
    pragmas=[
        ('journal_mode', 'wal'),
        ('foreign_keys', 1),
        ('autorollback', 1)
    ]
)


class BaseModel(Model):
    """Basemodel."""

    class Meta:
        database = DB_CONN


class Position(BaseModel):
    """Stores log_pos, log_file"""
    log_pos = IntegerField(default=None, null=False)
    log_file = CharField(default=None, null=False)
    streamed_at = TimestampField(null=False)

    @classmethod
    def truncate_if(cls, count: int):
        """Truncate x records when table > count."""
        raise cls.truncate_table()

    @classmethod
    def last(cls) -> Union[int, None]:
        """Retrieve last event.

        Only event successfully sent over Redis are saved.
        """
        try:
            lastitem = (cls.select().order_by(-cls.streamed_at)
                        .limit(1).execute())
            return lastitem[0]
        except IndexError:
            return None

    @classmethod
    def last_position(cls) -> int:
        """Last log position recorded."""
        last = cls.last()
        if isinstance(last, Position):
            return last.log_pos
        return None

    @classmethod
    def last_file(cls) -> str:
        """Last log file."""
        last = cls.last()
        if isinstance(last, Position):
            return last.log_file
        return None

    @classmethod
    def state(cls) -> int:
        """Count the records

        Used to decide:
            A) Resume a stream for previous point. OR
            B) Starting a stream; we have never streamed data.
        If value > 0; A, otherwise; B.

        """
        return cls.select().count()
