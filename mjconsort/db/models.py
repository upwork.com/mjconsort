from peewee import *
from .conn import BaseModel


class Hospital(BaseModel):
    address = CharField(null=True)
    branch = CharField(null=True)
    email = CharField(null=True)
    erp_token = CharField(null=True)
    have_branches = CharField(column_name='haveBranches', null=True)
    hospital_admin_email = CharField(column_name='hospitalAdminEmail', null=True)
    hospital_admin_name = CharField(column_name='hospitalAdminName', null=True)
    name = CharField(null=True)
    number_of_users = CharField(column_name='numberOfUsers', null=True)
    password = CharField(null=True)
    telephone = CharField(null=True)
    uuid = CharField(primary_key=True)
    ward = CharField(null=True)
    website_address = CharField(column_name='websiteAddress', null=True)

    class Meta:
        table_name = 'hospital'


class Devices(BaseModel):
    hospital_uuid = ForeignKeyField(column_name='hospitalUUID', field='uuid', model=Hospital)
    machine_id = CharField(column_name='machineId', null=True, unique=True)
    uuid = CharField(primary_key=True)

    class Meta:
        table_name = 'devices'


class Usergroups(BaseModel):
    description = CharField(null=True)
    name = CharField()
    uuid = CharField(primary_key=True)

    class Meta:
        table_name = 'usergroups'


class User(BaseModel):
    aadhaar_id = CharField(column_name='aadhaarId', null=True, unique=True)
    active = IntegerField(constraints=[SQL("DEFAULT 1")])
    address = CharField(null=True)
    age = IntegerField(null=True)
    biometric_uuid = CharField(column_name='biometricUUID', null=True)
    birth_date = DateTimeField(column_name='birthDate', null=True)
    center_id = BigIntegerField(column_name='centerId', null=True)
    city = CharField(null=True)
    creation_time = DateTimeField(column_name='creationTime', constraints=[SQL("DEFAULT current_timestamp()")], null=True)
    description = CharField(null=True)
    device_uuid = ForeignKeyField(column_name='deviceUUID', field='uuid', model=Devices, null=True)
    education = CharField(null=True)
    email = CharField(null=True, unique=True)
    first_name = CharField(column_name='firstName', null=True)
    hospital_uuid = ForeignKeyField(column_name='hospitalUUID', field='uuid', model=Hospital)
    last_name = CharField(column_name='lastName', null=True)
    name = CharField(null=True)
    password = CharField(constraints=[SQL("DEFAULT ''")])
    photo_uuid = CharField(column_name='photoUUID', constraints=[SQL("DEFAULT 'e4f4b0a6-2dc2-4e2e-873f-ee02cdf69ac7'")], null=True)
    preffered_pathologies = CharField(column_name='prefferedPathologies', null=True)
    preffered_pharmacies = CharField(column_name='prefferedPharmacies', null=True)
    preffered_radiologies = CharField(column_name='prefferedRadiologies', null=True)
    provider_number = CharField(column_name='providerNumber', null=True)
    role = CharField(null=True)
    sex = CharField(null=True)
    telephone = CharField(null=True)
    usergroup = ForeignKeyField(column_name='usergroup', field='uuid', model=Usergroups)
    username = CharField(constraints=[SQL("DEFAULT ''")])
    uuid = CharField(primary_key=True)
    vo_ip_number = CharField(column_name='voIP_number', null=True)

    class Meta:
        table_name = 'user'


class Patients(BaseModel):
    aadhaar_id = CharField(column_name='aadhaarId', unique=True)
    active = IntegerField(constraints=[SQL("DEFAULT 0")])
    address = CharField(null=True)
    age = IntegerField(constraints=[SQL("DEFAULT 0")])
    area = CharField(null=True)
    biometric_uuid = CharField(column_name='biometricUUID', null=True)
    birth_date = DateTimeField(column_name='birthDate', null=True)
    city = CharField(null=True)
    country = CharField(null=True)
    created_by_user_uuid = ForeignKeyField(column_name='createdByUserUUID', field='uuid', model=User)
    creation_time = DateTimeField(column_name='creationTime', constraints=[SQL("DEFAULT current_timestamp()")], null=True)
    device_uuid = ForeignKeyField(column_name='deviceUUID', field='uuid', model=Devices, null=True)
    email = CharField(null=True)
    first_name = CharField(column_name='firstName')
    hospital_uuid = ForeignKeyField(column_name='hospitalUUID', field='uuid', model=Hospital)
    last_name = CharField(column_name='lastName')
    martial_status = CharField(column_name='martialStatus', constraints=[SQL("DEFAULT 'unknown'")])
    name = CharField(null=True)
    photo_uuid = CharField(column_name='photoUUID', constraints=[SQL("DEFAULT 'e4f4b0a6-2dc2-4e2e-873f-ee02cdf69ac7'")], null=True)
    profession = CharField(constraints=[SQL("DEFAULT 'unknown'")])
    sex = CharField(constraints=[SQL("DEFAULT ' '")])
    sip_number = CharField(column_name='sipNumber', null=True)
    telephone = CharField(null=True)
    uuid = CharField(primary_key=True)

    class Meta:
        table_name = 'patients'


class ExaminationRecords(BaseModel):
    date = DateTimeField()
    patient_uuid = ForeignKeyField(column_name='patientUUID', field='uuid', model=Patients)
    uuid = CharField(primary_key=True)

    class Meta:
        table_name = 'examination_records'


class CalculatedParameters(BaseModel):
    hb = FloatField(column_name='Hb', null=True)
    o2_ct = FloatField(column_name='O2Ct', null=True)
    o2sat = FloatField(column_name='O2sat', null=True)
    ag = FloatField(null=True)
    ag_k = FloatField(null=True)
    be = FloatField(null=True)
    be_b = FloatField(null=True)
    be_ecf = FloatField(null=True)
    date = DateTimeField()
    hc_o3 = FloatField(column_name='hcO3', null=True)
    patient_uuid = ForeignKeyField(column_name='patientUUID', field='uuid', model=Patients)
    record_uuid = ForeignKeyField(column_name='recordUUID', field='uuid', model=ExaminationRecords, null=True)
    sbs = FloatField(null=True)
    tc_o2 = FloatField(column_name='tcO2', null=True)
    uuid = CharField(primary_key=True)

    class Meta:
        table_name = 'calculated_parameters'


class Electrolytes(BaseModel):
    cl = FloatField(column_name='Cl', null=True)
    li = FloatField(column_name='Li', null=True)
    date = DateTimeField()
    i_ca = FloatField(column_name='iCa', null=True)
    k = FloatField(null=True)
    na = FloatField(null=True)
    p_h = FloatField(column_name='pH', null=True)
    patient_uuid = ForeignKeyField(column_name='patientUUID', field='uuid', model=Patients)
    record_uuid = ForeignKeyField(column_name='recordUUID', field='uuid', model=ExaminationRecords, null=True)
    uuid = CharField(primary_key=True)

    class Meta:
        table_name = 'electrolytes'


class FlywaySchemaHistory(BaseModel):
    checksum = IntegerField(null=True)
    description = CharField()
    execution_time = IntegerField()
    installed_by = CharField()
    installed_on = DateTimeField(constraints=[SQL("DEFAULT current_timestamp()")])
    installed_rank = AutoField()
    script = CharField()
    success = IntegerField(index=True)
    type = CharField()
    version = CharField(null=True)

    class Meta:
        table_name = 'flyway_schema_history'


class HematologicalParameters(BaseModel):
    blood_gas = FloatField(null=True)
    blood_glucose = FloatField(null=True)
    date = DateTimeField()
    hct = FloatField(null=True)
    hemoglobin = FloatField(null=True)
    p_h = FloatField(column_name='pH', null=True)
    patient_uuid = ForeignKeyField(column_name='patientUUID', field='uuid', model=Patients)
    pc_o2 = FloatField(column_name='pcO2', null=True)
    po2 = FloatField(null=True)
    record_uuid = ForeignKeyField(column_name='recordUUID', field='uuid', model=ExaminationRecords, null=True)
    uuid = CharField(primary_key=True)

    class Meta:
        table_name = 'hematological_parameters'


class MeasurementTypes(BaseModel):
    classification = CharField()
    mt_index = IntegerField(null=True)
    name = CharField(null=True)
    uuid = CharField(primary_key=True)

    class Meta:
        table_name = 'measurement_types'


class MeasurementRequests(BaseModel):
    date = DateTimeField()
    device_uuid = ForeignKeyField(column_name='deviceUUID', field='uuid', model=Devices)
    measurement_type = ForeignKeyField(column_name='measurement_type', field='uuid', model=MeasurementTypes, null=True)
    patient_uuid = ForeignKeyField(column_name='patientUUID', field='uuid', model=Patients)
    status = IntegerField()
    type = CharField()
    uuid = CharField(primary_key=True)

    class Meta:
        table_name = 'measurement_requests'


class MeasurementsData(BaseModel):
    date = DateTimeField(null=True)
    patient_uuid = ForeignKeyField(column_name='patientUUID', field='uuid', model=Patients)
    type = ForeignKeyField(column_name='type', field='uuid', model=MeasurementTypes)
    uuid = CharField(primary_key=True)
    vale = FloatField(null=True)

    class Meta:
        table_name = 'measurements_data'


class Visits(BaseModel):
    active = IntegerField(constraints=[SQL("DEFAULT 1")])
    advisory = CharField(null=True)
    comments = CharField(null=True)
    created_by = CharField(column_name='createdBy', null=True)
    created_date = DateTimeField(column_name='createdDate', null=True)
    date = DateTimeField()
    examinations_prescribed = CharField(null=True)
    main_complaint_diagnosis1 = CharField(null=True)
    main_complaint_diagnosis2 = CharField(null=True)
    main_complaint_diagnosis3 = CharField(null=True)
    main_complaint_symptom = CharField(null=True)
    next_visit_date = DateTimeField(null=True)
    note = CharField(null=True)
    patient_uuid = ForeignKeyField(column_name='patientUUID', field='uuid', model=Patients)
    radiology_prescribed = CharField(null=True)
    secondary_complaint_diagnosis = CharField(null=True)
    sms = IntegerField(constraints=[SQL("DEFAULT 0")], null=True)
    timestamp = DateTimeField(constraints=[SQL("DEFAULT current_timestamp()")], null=True)
    type = CharField(constraints=[SQL("DEFAULT ''")], null=True)
    uuid = CharField(primary_key=True)

    class Meta:
        table_name = 'visits'


class MedicationPrescriptions(BaseModel):
    date = DateTimeField()
    duration = CharField(null=True)
    name = CharField(null=True)
    notes = CharField(null=True)
    patient_uuid = ForeignKeyField(column_name='patientUUID', field='uuid', model=Patients)
    time = CharField(null=True)
    uuid = CharField(primary_key=True)
    visit_uuid = ForeignKeyField(column_name='visitUUID', field='uuid', model=Visits, null=True)

    class Meta:
        table_name = 'medication_prescriptions'


class Metabolites(BaseModel):
    date = DateTimeField()
    glu = FloatField(null=True)
    lac = FloatField(null=True)
    patient_uuid = ForeignKeyField(column_name='patientUUID', field='uuid', model=Patients)
    record_uuid = ForeignKeyField(column_name='recordUUID', field='uuid', model=ExaminationRecords, null=True)
    uuid = CharField(primary_key=True)

    class Meta:
        table_name = 'metabolites'


class NursingStationData(BaseModel):
    blood_glucose = FloatField(null=True)
    blood_pressure_dia = IntegerField(null=True)
    blood_pressure_sys = IntegerField(null=True)
    datetime = DateTimeField(null=True)
    device_uuid = ForeignKeyField(column_name='deviceUUID', field='uuid', model=Devices)
    drip_flow = IntegerField(constraints=[SQL("DEFAULT 0")], null=True)
    future_tasks = CharField(null=True)
    heart_rate = IntegerField(null=True)
    o2_saturation = IntegerField(null=True)
    oxygen_flow = IntegerField(null=True)
    past_tasks = CharField(null=True)
    pathology = CharField(null=True)
    patient_uuid = ForeignKeyField(column_name='patientUUID', field='uuid', model=Patients)
    status = IntegerField(null=True)
    temperature = FloatField(null=True)
    uuid = CharField(primary_key=True)

    class Meta:
        table_name = 'nursing_station_data'


class Observations(BaseModel):
    consciousness = CharField(null=True)
    date = DateTimeField()
    patient_uuid = ForeignKeyField(column_name='patientUUID', field='uuid', model=Patients)
    record_uuid = ForeignKeyField(column_name='recordUUID', field='uuid', model=ExaminationRecords, null=True)
    smell = CharField(null=True)
    taste = CharField(null=True)
    uuid = CharField(primary_key=True)

    class Meta:
        table_name = 'observations'


class Pathology(BaseModel):
    data = TextField(null=True)
    date = DateTimeField(null=True)
    patient_uuid = ForeignKeyField(column_name='patientUUID', field='uuid', model=Patients)
    uuid = CharField(primary_key=True)

    class Meta:
        table_name = 'pathology'


class PatientFamilyDetails(BaseModel):
    father_name = CharField(column_name='fatherName', constraints=[SQL("DEFAULT ''")])
    father_status = IntegerField(column_name='fatherStatus', constraints=[SQL("DEFAULT 0")], null=True)
    hereditary_diseases = CharField(column_name='hereditaryDiseases', null=True)
    mother_name = CharField(column_name='motherName', constraints=[SQL("DEFAULT ''")])
    mother_status = IntegerField(column_name='motherStatus', constraints=[SQL("DEFAULT 0")], null=True)
    next_of_kin = CharField(column_name='nextOfKin', null=True)
    next_of_kin_relation_ship = CharField(column_name='nextOfKinRelationShip', null=True)
    next_of_kin_telephone = CharField(column_name='nextOfKinTelephone', null=True)
    parents_together = IntegerField(column_name='parentsTogether', constraints=[SQL("DEFAULT 0")], null=True)
    patient_uuid = ForeignKeyField(column_name='patientUUID', field='uuid', model=Patients)
    uuid = CharField(primary_key=True)

    class Meta:
        table_name = 'patient_family_details'


class PatientHabits(BaseModel):
    habit_frequency = IntegerField(column_name='habitFrequency', null=True)
    habit_name = CharField(column_name='habitName')
    habit_years = IntegerField(column_name='habitYears', null=True)
    patient_uuid = ForeignKeyField(column_name='patientUUID', field='uuid', model=Patients)
    uuid = CharField(primary_key=True)

    class Meta:
        table_name = 'patient_habits'


class PatientHealthCards(BaseModel):
    number = CharField()
    patient_uuid = ForeignKeyField(column_name='patientUUID', field='uuid', model=Patients)
    type = CharField()
    uuid = CharField(primary_key=True)

    class Meta:
        table_name = 'patient_health_cards'


class PatientInsurances(BaseModel):
    company = CharField()
    number = CharField()
    patient_uuid = ForeignKeyField(column_name='patientUUID', field='uuid', model=Patients)
    uuid = CharField(primary_key=True)

    class Meta:
        table_name = 'patient_insurances'


class PatientMedicalInformation(BaseModel):
    allergies = CharField(null=True)
    blood_type = CharField(column_name='bloodType', constraints=[SQL("DEFAULT 'Unknown'")])
    existing_medication = CharField(column_name='existingMedication', null=True)
    nature_of_medical_condition = CharField(column_name='natureOfMedicalCondition', null=True)
    patient_uuid = ForeignKeyField(column_name='patientUUID', field='uuid', model=Patients)
    previous_medical_conditions = CharField(column_name='previousMedicalConditions', null=True)
    uuid = CharField(primary_key=True)

    class Meta:
        table_name = 'patient_medical_information'


class PatientSurgeries(BaseModel):
    doctor = CharField(null=True)
    name = CharField()
    patient_uuid = ForeignKeyField(column_name='patientUUID', field='uuid', model=Patients)
    reason = CharField(null=True)
    uuid = CharField(primary_key=True)
    year = CharField()

    class Meta:
        table_name = 'patient_surgeries'


class Pharmacies(BaseModel):
    address = CharField(null=True)
    city = CharField(null=True)
    email = CharField(null=True)
    name = CharField(null=True)
    telephone = CharField(null=True)
    uuid = CharField(primary_key=True)

    class Meta:
        table_name = 'pharmacies'


class PhotoExaminations(BaseModel):
    date = DateTimeField()
    description = CharField(null=True)
    patient_uuid = ForeignKeyField(column_name='patientUUID', field='uuid', model=Patients)
    photo = CharField(null=True)
    record_uuid = ForeignKeyField(column_name='recordUUID', field='uuid', model=ExaminationRecords, null=True)
    type = CharField()
    uuid = CharField(primary_key=True)

    class Meta:
        table_name = 'photo_examinations'


class PhysicalMeasurements(BaseModel):
    date = DateTimeField()
    height = FloatField(null=True)
    patient_uuid = ForeignKeyField(column_name='patientUUID', field='uuid', model=Patients)
    record_uuid = ForeignKeyField(column_name='recordUUID', field='uuid', model=ExaminationRecords, null=True)
    uuid = CharField(primary_key=True)
    weight = FloatField(null=True)

    class Meta:
        table_name = 'physical_measurements'


class PhysiologicalParameters(BaseModel):
    blood_pressure_dia = IntegerField(null=True)
    blood_pressure_sys = IntegerField(null=True)
    date = DateTimeField()
    heart_rate = IntegerField(null=True)
    o2_flow_rate = IntegerField(null=True)
    o2_saturation = IntegerField(null=True)
    patient_uuid = ForeignKeyField(column_name='patientUUID', field='uuid', model=Patients)
    record_uuid = ForeignKeyField(column_name='recordUUID', field='uuid', model=ExaminationRecords, null=True)
    respiratory_rate = IntegerField(null=True)
    temperature = FloatField(null=True)
    uuid = CharField(primary_key=True)

    class Meta:
        table_name = 'physiological_parameters'


class Vaccinations(BaseModel):
    date = DateTimeField()
    name = CharField()
    patient_uuid = ForeignKeyField(column_name='patientUUID', field='uuid', model=Patients)
    uuid = CharField(primary_key=True)

    class Meta:
        table_name = 'vaccinations'

