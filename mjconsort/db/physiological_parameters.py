from peewee import IntegerField
from peewee import DateTimeField
from peewee import CharField
from peewee import DoubleField

from .conn import BaseModel


class physiological_parameters(BaseModel):
    uuid = CharField(max_length=36, null=False)
    respiratory_rate = IntegerField(null=True)
    o2_saturation = IntegerField(null=True)
    blood_pressure_sys = IntegerField(null=True)
    blood_pressure_dia = IntegerField(null=True)
    heart_rate = IntegerField(null=True)
    temperature = DoubleField(null=True)
    date = DateTimeField(null=False)
    patientUUID = CharField(max_length=36, null=False)