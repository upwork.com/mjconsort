from peewee import BlobField
from peewee import CharField
from peewee import IntegerField
from peewee import DateTimeField

from .conn import BaseModel


class pathology(BaseModel):
    uuid = CharField(max_length=36, null=False)
    date = DateTimeField(null=True)
    data = BlobField(null=True)
    patientUUID = CharField(max_length=36, null=False)
