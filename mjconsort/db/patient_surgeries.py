from peewee import IntegerField
from peewee import CharField, ForeignKeyField


from .conn import BaseModel
from .patients import patients


class patient_surgeries(BaseModel):
    uuid = CharField(max_length=36, null=False, unique=True, primary_key=True)
    name = CharField(null=False)
    year = CharField(null=False)
    reason = CharField(null=False)
    doctor = CharField(null=False)
    patientUUID = CharField(max_length=36, null=False)