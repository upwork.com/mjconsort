from peewee import CharField, ForeignKeyField

from .conn import BaseModel
from .patients import patients

class patient_insurances(BaseModel):
    uuid = CharField(max_length=36, null=False)
    company = CharField(null=False)
    number = CharField(null=False)
    patientUUID = CharField(max_length=36, null=False)