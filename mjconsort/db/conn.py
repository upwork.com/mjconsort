import os

from peewee import Model
from peewee import MySQLDatabase


DB_CONN = MySQLDatabase(
    host=os.environ['DB_HOST'],
    user=os.environ['DB_USER'],
    password=os.environ['DB_PASS'],
    database=os.environ['DB_NAME'],
    charset='utf8mb4'
)


class BaseModel(Model):
    """Basemodel."""

    class Meta:
        database = DB_CONN

    # host='127.0.0.1',
    # user='mjconsort',
    # password='mariadb',
    # database='mjconsort',
    # charset='utf8mb4'