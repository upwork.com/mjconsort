from peewee import CharField
from peewee import DateTimeField
from peewee import DoubleField

from .conn import BaseModel


class physical_measurements(BaseModel):
    uuid = CharField(max_length=36, null=False)
    height = DoubleField(null=True)
    weight = DoubleField(null=True)
    date = DateTimeField(null=False)
    patientUUID = CharField(null=False)