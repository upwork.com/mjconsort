"""Create, drop, and truncate tables"""
import logging

from .conn import DB_CONN

from .visits import visits
from .patients import patients
from .user import user
from .physiological_parameters import physiological_parameters
from .physical_measurements import physical_measurements
from .photo_examinations import photo_examinations
from .patient_surgeries import patient_surgeries
from .patient_medical_information import patient_medical_information
from .patient_insurances import patient_insurances
from .patient_health_cards import patient_health_cards
from .patient_habits import patient_habits
from .patient_family_details import patient_family_details
from .pathology import pathology
from .observations import observations
from .nursing_station_data import nursing_station_data
from .flyway_schema_history import flyway_schema_history
from .metabolites import metabolites
from .measurements_data import measurements_data
from .hospital import hospital
from .hematological_parameters import hematological_parameters
from .electrolytes import electrolytes
from .devices import devices
from .calculated_parameters import calculated_parameters
from .usergroups import usergroups
from .pharmacies import pharmacies
from .measurements_types import measurements_types

# Binlog position
from .position import Position


logging.basicConfig(level=logging.INFO)


class Model:
    TABLES = [
        hospital,
        devices,
        usergroups,
        user,
        patients,
        physiological_parameters,
        physical_measurements,
        photo_examinations,
        patient_surgeries,
        patient_medical_information,
        patient_insurances,
        patient_health_cards,
        patient_habits,
        patient_family_details,
        pathology,
        observations,
        nursing_station_data,
        flyway_schema_history,
        metabolites,
        measurements_data,
        visits,
        hematological_parameters,
        electrolytes,
        calculated_parameters,
        pharmacies,
        measurements_types,
        Position
    ]

    @classmethod
    def check_all(cls):
        """Create tables if not exists."""
        for table in cls.TABLES:
            if table.table_exists():
                logging.info(f'Table {table._meta.table_name} exists')
            else:
                table.create_table()
                logging.info(f'Table {table._meta.table_name} created')

    @classmethod
    def drop_all(cls):
        """Drop every table in the database."""
        logging.info('Dropping all tables now!')
        try:
            DB_CONN.execute_sql("SET FOREIGN_KEY_CHECKS=0")
            for table in cls.TABLES:
                table.drop_table()
                logging.info(f'dropping table {table}')
        except Exception as error:
            logging.info(f'I found a bug {error} for table {table}')