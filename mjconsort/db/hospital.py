from peewee import CharField

from .conn import BaseModel


class hospital(BaseModel):
    uuid = CharField(max_length=36, null=False, unique=True, primary_key=True)
    name = CharField(null=True)
    branch = CharField(null=True)
    ward = CharField(null=True)
    address = CharField(null=True)
    email = CharField(null=True)
    password = CharField(null=True)
    telephone = CharField(null=True)
    websiteAddress = CharField(null=True)
    haveBranches = CharField(null=True)
    numberOfUsers = CharField(null=True)
    erp_token = CharField(null=True)
    hospitalAdminName = CharField(null=True)
    hospitalAdminEmail = CharField(null=True)