from peewee import CharField
from peewee import IntegerField
from peewee import DoubleField
from peewee import DateTimeField

from .conn import BaseModel


class measurements_data(BaseModel):
    uuid = CharField(max_length=36, null=False)
    type = IntegerField(null=True)
    vale = DoubleField(null=True)
    date = DateTimeField(null=False)
    patientUUID = CharField(max_length=36, null=False)