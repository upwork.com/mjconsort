from peewee import CharField, ForeignKeyField
from .conn import BaseModel
from .hospital import hospital


class devices(BaseModel):
    uuid = CharField(max_length=36, null=False, unique=True, primary_key=True)
    machineId = CharField(null=True)
    hospitalUUID = CharField(max_length=36, null=False)