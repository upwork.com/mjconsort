from peewee import CharField
from peewee import SmallIntegerField, ForeignKeyField


from .conn import BaseModel

from .patients import patients


class patient_family_details(BaseModel):
    uuid = CharField(max_length=36, null=False, unique=True, primary_key=True)
    motherName = CharField(null=False)
    motherStatus = SmallIntegerField(null=False, default=0)
    fatherName = CharField(null=False)
    fatherStatus = SmallIntegerField(null=False, default=0)
    parentsTogether = SmallIntegerField(default=0)
    nextOfKin = CharField(null=True)
    hereditaryDiseases = CharField(null=True)
    nextOfKinRelationShip = CharField(null=True)
    nextOfKinTelephone = CharField(null=True)
    patientUUID = ForeignKeyField(patients)
    