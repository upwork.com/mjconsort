from peewee import DoubleField
from peewee import DateTimeField
from peewee import CharField


from .conn import BaseModel


class electrolytes(BaseModel):
    uuid = CharField(max_length=36, null=False, unique=True, primary_key=True)
    na = DoubleField(null=True)
    k = DoubleField(null=True)
    iCa = DoubleField(null=True)
    Li = DoubleField(null=True)
    pH = DoubleField(null=True)
    Cl = DoubleField(null=True)
    date = DateTimeField(null=False)
    patientUUID = CharField(max_length=36, null=False)