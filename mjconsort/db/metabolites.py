from peewee import IntegerField
from peewee import DateTimeField
from peewee import DoubleField
from peewee import CharField

from .conn import BaseModel


class metabolites(BaseModel):
    uuid = CharField(max_length=36, null=False)
    glu = DoubleField(null=True)
    lac = DoubleField(null=True)
    date = DateTimeField(null=False)
    patientUUID = CharField(max_length=36, null=False)