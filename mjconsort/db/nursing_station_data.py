from peewee import CharField
from peewee import SmallIntegerField
from peewee import DateTimeField
from peewee import IntegerField
from peewee import DoubleField


from .conn import BaseModel


class nursing_station_data(BaseModel):
    uuid = CharField(max_length=36, null=False)
    patientUUID = CharField(max_length=36, null=False)
    deviceUUID = CharField(max_length=36, null=False)
    heart_rate = IntegerField(null=True)
    o2_saturation = IntegerField(null=True)
    blood_pressure_sys = IntegerField(null=True)
    blood_pressure_dia = IntegerField(null=True)
    datetime = DateTimeField(null=True)
    past_tasks = CharField(null=True)
    future_tasks = CharField(null=True)
    pathology = CharField(null=True)
    blood_glucose = DoubleField(null=True)
    temperature = DoubleField(null=True)
    drip_flow = SmallIntegerField(null=False, default=0)
    oxygen_flow = IntegerField(null=True)