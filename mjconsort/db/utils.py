from .differentcentre import different_centre
from .examinations import examinations
from .patientexaminations import patient_examinations
from .patient import patient
from .measurementsdata import measurements_data
from .measurementtypes import measurement_types
from .pathology import pathology
from .usergroup import usergroup
from .devicedetails import device_details
from .nursingstationdata import nursing_station_data
from .visits import visits
from .user import user

tables = [
    patient,
    device_details,
    examinations,
    different_centre,
    patient_examinations,
    pathology,
    measurement_types,
    measurements_data,
    usergroup,
    nursing_station_data,
    visits,
    user
]


class SchemaUtil:
    @staticmethod
    def droptables():
        for table in tables:
            try:
                table.drop_table()
                print('dropped', table)
            except Exception as error:
                print(table, error)
    @staticmethod
    def createtables():
        for table in tables:
            try:
                table.create_table()
                print('created', table)
            except Exception as error:
                print(table, error)

