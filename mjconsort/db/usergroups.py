from peewee import CharField

from .conn import BaseModel


class usergroups(BaseModel):
    uuid = CharField(null=False, primary_key=True, unique=True)
    name = CharField(null=True)
    description = CharField(null=True)