from peewee import CharField, ForeignKeyField

from .conn import BaseModel
from .patients import patients


class patient_medical_information(BaseModel):
    uuid = CharField(max_length=36, null=False, unique=True, primary_key=True)
    bloodType = CharField(null=False)
    previousMedicalConditions = CharField(null=True)
    natureOfMedicalCondition = CharField(null=True)
    existingMedication = CharField(null=True)
    allergies = CharField(null=True)
    patientUUID = ForeignKeyField(patients)