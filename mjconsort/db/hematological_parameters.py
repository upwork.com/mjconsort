from peewee import DoubleField
from peewee import DateTimeField
from peewee import CharField

from .conn import BaseModel

class hematological_parameters(BaseModel):
    uuid = CharField(max_length=36, null=False, unique=True, primary_key=True)
    blood_glucose = DoubleField(null=True)
    hemoglobin = DoubleField(null=True)
    blood_gas = DoubleField(null=True)
    pH = DoubleField(null=True)
    pcO2 = DoubleField(null=True)
    po2 = DoubleField(null=True)
    hct = DoubleField(null=True)
    date = DateTimeField(null=False)
    patientUUID = CharField(max_length=36, null=False)