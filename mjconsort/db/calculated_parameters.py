from peewee import CharField
from peewee import DateTimeField
from peewee import DoubleField

from .conn import BaseModel

class calculated_parameters(BaseModel):
    uuid = CharField(max_length=36, null=False)
    Hb = DoubleField(null=True)
    hcO3 = DoubleField(null=True)
    be = DoubleField(null=True)
    be_b = DoubleField(null=True)
    be_ecf = DoubleField(null=True)
    tcO2 = DoubleField(null=True)
    ag = DoubleField(null=True)
    O2sat = DoubleField(null=True)
    O2Ct = DoubleField(null=True)
    sbs = DoubleField(null=True)
    date = DateTimeField(null=False)
    patientUUID = CharField(max_length=36, null=False)