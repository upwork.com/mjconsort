from enum import unique
from peewee import CharField
from peewee import DateTimeField
from peewee import SmallIntegerField
from peewee import BigIntegerField
from peewee import IntegerField
from peewee import TimestampField
from peewee import ForeignKeyField

from .conn import BaseModel

from .devices import devices
from .usergroups import usergroups
from .hospital import hospital


class user(BaseModel):
    uuid = CharField(null=False, primary_key=True, unique=True)
    usergroup = CharField(max_length=36, null=False)
    username = CharField(null=False, default='')
    password = CharField(null=False, default='')
    description = CharField(null=True)
    creationTime = TimestampField(null=True)
    active = SmallIntegerField(null=False, default=1)
    email = CharField(null=True)
    age = IntegerField(null=True)
    birthDate = DateTimeField(null=True)
    sex = CharField(max_length=4, null=False)
    address = CharField(null=True)
    city = CharField(null=True)
    telephone = CharField(null=True)
    aadhaarId = CharField(null=True)
    education = CharField(null=True)
    photoUUID = CharField(null=True, default='e4f4b0a6-2dc2-4e2e-873f-ee02cdf69ac7')
    firstName = CharField(null=True)
    lastName = CharField(null=True)
    name = CharField(null=True)
    providerNumber = CharField(null=True)
    role = CharField(null=True)
    biometricUUID = CharField(max_length=36, null=True)
    voIP_number = CharField(null=True)
    hospitalUUID = CharField(max_length=36, null=False)
    deviceUUID = CharField(max_length=36, null=False)
    prefferedPathologies = CharField(null=True)
    prefferedPharmacies = CharField(null=True)
    prefferedRadiologies = CharField(null=True)
    centerId = BigIntegerField(null=True)