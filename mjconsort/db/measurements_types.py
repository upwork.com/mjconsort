from peewee import CharField
from peewee import IntegerField

from .conn import BaseModel

class measurements_types(BaseModel):
    uuid = CharField(max_length=36, null=False)
    name = CharField(null=True)
    mt_index = IntegerField(null=True)
