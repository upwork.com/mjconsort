from peewee import CharField, ForeignKeyField

from .conn import BaseModel
from .patients import patients


class patient_health_cards(BaseModel):
    uuid = CharField(max_length=36, null=False, unique=True, primary_key=True)
    type = CharField(null=False)
    number = CharField(null=True)
    patientUUID = CharField(max_length=36, null=False)
