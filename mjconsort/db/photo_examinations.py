from peewee import CharField
from peewee import DateTimeField

from .conn import BaseModel


class photo_examinations(BaseModel):
    uuid = CharField(max_length=36, null=False)
    description = CharField()
    type = CharField(null=False)
    photo = CharField(null=True)
    date = DateTimeField(null=False)
    patientUUID = CharField(null=False)