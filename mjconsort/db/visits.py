from peewee import CharField
from peewee import DateTimeField
from peewee import SmallIntegerField
from peewee import TimestampField

from .conn import BaseModel



class visits(BaseModel):
    uuid = CharField(null=False)
    patientUUID = CharField(null=False)
    date = DateTimeField(null=False)
    note = CharField(null=True)
    sms = SmallIntegerField(null=False, default=0)
    createdBy = CharField(null=True)
    createdDate = DateTimeField(null=True)
    active = SmallIntegerField(null=False, default=1)
    type = CharField(max_length=1, default='')
    main_complaint_symptom = CharField(null=True)
    main_complaint_diagnosis1 = CharField(null=True)
    main_complaint_diagnosis2 = CharField(null=True)
    main_complaint_diagnosis3 = CharField(null=True)
    secondary_complaint_diagnosis = CharField(null=True)
    timestamp = TimestampField(null=True)
    examinations_prescribed = CharField(null=True)
    advisory = CharField(null=True)
    next_visit_date = DateTimeField(null=True)
    comments = CharField(null=True)
    medications_prescribed = CharField(null=True)
    radiology_prescribed = CharField(null=True)
