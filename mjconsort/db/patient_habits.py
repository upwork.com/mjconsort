from peewee import CharField
from peewee import IntegerField, ForeignKeyField


from .conn import BaseModel
from .patients import patients


class patient_habits(BaseModel):
    uuid = CharField(max_length=36, null=False, unique=True, primary_key=True)
    habitName = CharField(null=False)
    habitYears = IntegerField(null=True)
    habitFrequency = IntegerField(null=True)
    patientUUID = CharField(max_length=36, null=False)