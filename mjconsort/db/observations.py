from peewee import CharField
from peewee import DateTimeField

from .conn import BaseModel


class observations(BaseModel):
    uuid = CharField(max_length=36, null=False)
    taste = CharField(null=False)
    smell = CharField(null=False)
    consciousness = CharField(null=False)
    date = DateTimeField(null=False)
    patientUUID = CharField(max_length=36, null=False)