from peewee import CharField
from peewee import DateTimeField
from peewee import TimestampField
from peewee import SmallIntegerField
from peewee import ForeignKeyField

from .conn import BaseModel

from .devices import devices
from .usergroups import usergroups
from .hospital import hospital
from .user import user


class patients(BaseModel):
    uuid = CharField(null=False, unique=True, primary_key=True)
    aadhaarId = CharField(null=False)
    firstName = CharField(null=False)
    lastName = CharField(null=False)
    name = CharField(null=True)
    birthDate = DateTimeField(null=True)
    age = SmallIntegerField(null=False, default=0)
    sex = CharField(max_length=4, null=False, default='')
    address = CharField(null=True)
    sipNumber = CharField(null=True)
    city = CharField(null=False)
    email = CharField(null=False)
    telephone = CharField(null=False)
    creationTime = TimestampField(null=True)
    createdByUserUUID = CharField(max_length=36, null=False)
    active = SmallIntegerField(null=False, default=0)
    profession = CharField(null=False, default='unknown')
    martialStatus = CharField(null=False, default='unknown')
    area = CharField(null=True)
    country = CharField(null=True)
    biometricUUID = CharField(default=True)
    photoUUID = CharField(max_length=36, default='e4f4b0a6-2dc2-4e2e-873f-ee02cdf69ac7')
    hospitalUUID = CharField(max_length=36, null=False)
    deviceUUID = CharField(max_length=36, null=False)
