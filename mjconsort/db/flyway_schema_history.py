from datetime import datetime

from peewee import IntegerField
from peewee import CharField
from peewee import TimestampField


from .conn import BaseModel


class flyway_schema_history(BaseModel):
    installed_rank = IntegerField(null=True)
    version = CharField(null=True, max_length=50)
    description = CharField(null=False, max_length=200)
    type = CharField(null=False, max_length=200)
    script = CharField(null=False, max_length=200)
    checksum = IntegerField(null=True)
    installed_by = CharField(null=False, max_length=200)
    installed_on = TimestampField(null=False, default=datetime.now().timestamp)
    execution_time = IntegerField(null=False)
    success = IntegerField(null=False)
