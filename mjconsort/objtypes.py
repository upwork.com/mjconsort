import dataclasses
import typing
from datetime import datetime

import peewee


@dataclasses.dataclass
class TPosition:
    log_file: typing.Union[str, None] = None
    log_pos: typing.Union[str, None] = None
    timestamp: typing.Union[float, None] = None

    def asdict(self):
        """Return TBinLogEvent as a type:dict"""
        return dataclasses.asdict(self)


@dataclasses.dataclass
class WriteEvent:
    event_type: str = 'WriteEvent'


@dataclasses.dataclass
class DeleteEvent:
    event_type: str = 'DeleteEvent'


@dataclasses.dataclass
class UpdateEvent:
    event_type: str = 'UpdateEvent'


@dataclasses.dataclass
class UnknownEvent:
    event_type: str = 'UnknownEvent'


@dataclasses.dataclass
class BinaryLogEvent:
    """Binary log event.

    """
    schema: typing.Union[None, str] = None
    row: typing.Union[None, dict] = None
    table: typing.Union[None, str] = None
    primary_key: typing.Any = None
    columns: typing.Union[list, None] = None
    timestamp: typing.Union[float, None] = None
    event_type: int = 0
    position: typing.Union[TPosition, None] = None

    def asdict(self):
        """Return TBinLogEvent as a type:dict"""
        return dataclasses.asdict(self)


@dataclasses.dataclass
class TRow:
    """Row change data.

    Delete and Update events return two sets of
    data; after_value - after change is made and
    before_value - data before change is made.
    """
    values: typing.Union[dict, None] = None
    after_values: typing.Union[dict, None] = None
    before_values: typing.Union[dict, None] = None

    def asdict(self):
        """Return TRow as a type:dict"""
        return dataclasses.asdict(self)


@dataclasses.dataclass
class TTableProperties:
    """Table properties."""
    table: typing.Union[peewee.ModelBase, None]
    pk: typing.Union[str, None] = None
    pk_type: typing.Union[str, None] = None
    pk_name: typing.Union[str, None] = None

    def __post_init__(self):
        self.pk = self.table._meta.primary_key
        self.pk_type = self.table._meta.primary_key.field_type
        self.pk_name = self.table._meta.primary_key.column_name
