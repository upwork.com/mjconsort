"""MAIN CONFIG"""

import os
import re

# Base Path
BASE_PATH = os.path.abspath(os.path.join(os.path.dirname(__file__), os.pardir))
LOG_DIR = BASE_PATH + '/mjconsort/logs/'

# Database Mysql
DB_SETTINGS = {
    "host": os.environ['DB_HOST'],
    "port": 3306,
    "user": os.environ['DB_USER'],
    "passwd": os.environ['DB_PASS'],
    'db': os.environ['DB_NAME']
}


# Redis
REDIS_SETTINGS = {
    'SERVER': {
        "host": os.environ['REDIS_HOST'],
        "port": 6379,
        "db": 0
    },
    'KEY': 'default'
}
