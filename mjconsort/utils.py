import datetime
import json
from typing import Union
import logging
from logging import handlers
from mjconsort.objtypes import TRow
from mjconsort.objtypes import DeleteEvent
from mjconsort.objtypes import UnknownEvent
from mjconsort.objtypes import WriteEvent
from mjconsort.objtypes import UpdateEvent
from mjconsort import config


def event_name(event_type: int):
    """Event name.

    Args:
        int(event_type): nos representing event_type

    Returns:
        str
    """

    event_names = {
        23: 'WriteEvent',
        30: 'WriteEvent',
        24: 'UpdateEvent',
        31: 'UpdateEvent',
        25: 'DeleteEvent',
        32: 'DeleteEvent',
        0:  'UnknownEvent',
    }
    return event_names[event_type]


def dt_to_iso(row):
    """Cast datetime.datetime to iso format."""
    for item in row.items():
        if isinstance(item[1], (datetime.datetime, datetime.date)):
            row[item[0]] = item[1].isoformat()
    return row


def parse_row(row: dict, event_type: int):
    """Parse row according to event type."""
    if event_type in (23, 25, 30, 32, 'DeleteEvent', 'WriteEvent'):
        return TRow(values=dt_to_iso(row['values']))
    if event_type in (24, 31, 'UpdateEvent'):
        return TRow(
            before_values=dt_to_iso(row['before_values']),
            after_values=dt_to_iso(row['after_values'])
        )


def event_type(event_type: Union[int, str]):
    """An event_type instance."""
    event_types = {
            23: WriteEvent,
            30: WriteEvent,
            24: UpdateEvent,
            31: UpdateEvent,
            25: DeleteEvent,
            32: DeleteEvent,
            0:  UnknownEvent,
            'WriteEvent': WriteEvent,
            'UpdateEvent': UpdateEvent,
            'DeleteEvent': DeleteEvent,
            'UnknownEvent': UnknownEvent
    }

    return event_types[event_type]()


class Logger:
    @staticmethod
    def start():
        logging.basicConfig(
            handlers=[
                handlers.RotatingFileHandler(
                    config.LOG_DIR + '/mjconsort.log',
                    mode='a',
                    maxBytes=10**7,
                    backupCount=2
                ),
                logging.StreamHandler(),
            ],
            format='[MJCONSORT] %(asctime)s - %(message)s',
            level=logging.INFO,
            datefmt='%Y-%m-%d %H:%M %S'
        )
