"""MJCONSORT.PRODUCER.POSITION"""
from mjconsort.db.position import Position
from mjconsort.objtypes import TPosition


class LogPosition:
    """Provides access to log_pos and log_file."""
    def __init__(self):
        self.position = Position
        self.state = self.position.state()

    def save_position(self, position: TPosition):
        """Save the position of the last log event.

        Only event successfully sent to the consumer should
        be saved. If you do otherwise, you risk streaming
        from the incorrect position relative to last event
        :BinaryLogReader processed.

        https://github.com/noplay/python-mysql-replication/issues/252
        """
        self.position.create(**position.asdict()).save()

    def get_pos(self):
        """Last processed log position."""
        if self.state == 0:
            return None
        return self.position.last_position()

    def get_file(self):
        """Last known binary log file."""
        if self.state == 0:
            return None
        return self.position.last_file()
