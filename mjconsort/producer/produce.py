"""Binary log reader

Reads the Binary Log, parses the event, envelopes the event
in a message, sends the message.
"""

import random
import logging

from pymysqlreplication import BinLogStreamReader
from pymysqlreplication.row_event import DeleteRowsEvent
from pymysqlreplication.row_event import UpdateRowsEvent
from pymysqlreplication.row_event import WriteRowsEvent

from mjconsort.objtypes import BinaryLogEvent
from mjconsort.objtypes import TPosition
from mjconsort.producer.position import LogPosition
from mjconsort.message import WithRedis
from mjconsort import config
from mjconsort import utils

utils.Logger.start()


class EventManager:
    """Process an event.

    Sends event to a remote Redis server; if the operation is successful,
    the log_pos:log_file is saved to an sqlite database. The
    values are saved to sqlite are used to resume streaming or
    failure recovery.

    """
    def __init__(self):
        self.log_position = LogPosition()
        self.messenger = WithRedis().connect()

    def process(self, event) -> None:
        """Parse  an event.

        1. Send event
        2. Update log_file, log_pos
        """
        status = False
        if event:
            try:
                status = self.messenger.send(event)
            except AttributeError as e:
                logging.info(e)
            if status:
                self.log_position.save_position(position=event.position)


class ServerID():
    """Generates a 32bit int.

    https://mariadb.com/products/skysql/docs/reference/es/system-variables/server_id/

    """
    @staticmethod
    def get_rand():
        MIN_SERVER_ID = 1
        MAX_SERVER_ID = 4294967295
        return random.randint(MIN_SERVER_ID, MAX_SERVER_ID)


class BinaryLogReader:
    """Read events from a Binary Log.

    The binary log records statements that modify data
    in the database, it however, does not record statement
    which have no effect on the data . If an UPDATE
    or DELETE command has no effect on data, the binary log
    will not have record the event - it will be ommitted.
    SELECT commands are also ommitted.

    Enable binary logs in /etc/mysql/my.cnf; The row format
    is recommend.
    """
    def __init__(self):
        self.mysql_settings = config.DB_SETTINGS
        self.server_id = ServerID.get_rand()
        self.event_manager = EventManager()

    def process(self):
        """Stream events from the Binary Log."""
        stream = BinLogStreamReader(
            self.mysql_settings,
            self.server_id,
            blocking=True,
            only_events=[DeleteRowsEvent, UpdateRowsEvent, WriteRowsEvent],
            resume_stream=True,
            log_file=LogPosition().get_file(),
            log_pos=LogPosition().get_pos()
        )

        while True:
            streamobj = stream.fetchone()
            if streamobj:
                for row in streamobj.rows:
                    event = BinaryLogEvent(
                        schema=streamobj.schema,
                        row=utils.parse_row(row, streamobj.event_type),
                        table=streamobj.table,
                        timestamp=streamobj.timestamp,
                        primary_key=streamobj.primary_key,
                        event_type=streamobj.event_type,
                        position=TPosition(
                            log_pos=streamobj.packet.log_pos,
                            log_file=stream.log_file,
                            timestamp=streamobj.timestamp
                        )
                    )
                    logging.info(
                        (f'BinaryLogReader - Found {utils.event_name(event.event_type)} on log_pos:'
                         f'{event.position.log_pos} TABLE:{event.table}')
                    )
                    self.event_manager.process(event)