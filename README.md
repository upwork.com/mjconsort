# MJConsort

Multi-Source Replication for MedJacket.

| Milestone      | Status |
| ----------- | ----------- |
| Bin Log Parsing and Streaming      | COMPLETED       |
| Replaying of change data on replay-server   | COMPLETED        |
| Integration   | PENDING        |
| Deliverables   | PENDING        |


### Currently Available Features

- Parse MySQL Binary Log into events (tested with MariaDB)
- Persist log_pos, log_file to sqlite
- Streams/send as message using Redis
- Restarts at last streamed/sent event
- Replay change-data on remote server
    - Write OK
    - Delete OK
    - Update OK

### Required
- Python 3.7
- MariaDB 10
- Redis
- Debian/Ubuntu

### Milestone 1
#### https://asciinema.org/a/5L6rFBcvbqhOoUZIHt6QHAqun

![milestoneone](milestone-one.gif)


### Milestone 2
#### https://asciinema.org/a/sozTSV7IGRcizwvk1wF1l4qVo
![milestoneone](milestone-two.gif)