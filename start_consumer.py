from threading import Thread

from mjconsort.consumer.consume import get_event_from_queue
from mjconsort.consumer.consume import replay_event

def consumer():
    while True:
        replay_event(get_event_from_queue())

if __name__ == "__main__":
    thread = Thread(target=consumer)
    thread.start()
