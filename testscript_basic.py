#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import pandas
import os
import logging
import time
from typing import NamedTuple, Any
import peewee

logging.basicConfig(level=logging.INFO)


from mjconsort.schema.patients import patients
from mjconsort.schema.devices import devices
from mjconsort.schema.hospital import hospital
from mjconsort.schema.usergroups import usergroups
from mjconsort.schema.user import user
from mjconsort.schema.patient_family_details import patient_family_details
from mjconsort.schema.patient_habits import patient_habits
from mjconsort.schema.patient_insurances import patient_insurances
from mjconsort.schema.patient_surgeries import patient_surgeries
from mjconsort.schema.patient_health_cards import patient_health_cards


def get_files():
    return [file.path for file in os.scandir('testdata')]

def yesto1(w):
    if w == 'Yes':
        return 1
    return 0

def p_age(w):
    return w.split('years')[0].strip()

def tstamp_tdate(w):
    return w.date().__str__()

def known_hospital(v):
    return '59b254ff-7ba6-4c1a-82c8-356fe145dea6'

def known_device(v):
    return 'de542181-5960-4cea-b423-e5ed5decf541'

def known_user(v):
    return '2bcf41d7-849e-47c2-9361-845bebc621d4'

def tstamp2(v):
    return None

class Demo:

    @staticmethod
    def do():
        files = get_files()
        for file in files:
            logging.info(f'Reading a spreadsheet with name {file}!')


            # patient
            healthdata = pandas.ExcelFile(file)
            patient_df = pandas.read_excel(healthdata, 'patients').fillna('')
            patient_df['birthDate'] = patient_df.birthDate.apply(tstamp_tdate)
            patient_df['deviceUUID'] = patient_df.deviceUUID.apply(known_device)                                              
            patient_df['hospitalUUID'] = patient_df.hospitalUUID.apply(known_hospital)                                        
            patient_df['createdByUserUUID'] = patient_df.createdByUserUUID.apply(known_user)
            patient_df['creationTime'] = patient_df.creationTime.apply(tstamp2)
            patient_list = patient_df.to_dict(orient='records')
            for p in patient_list:
                try:
                    patients.insert(p).execute()
                    logging.info(f'PATIENT  {p["name"]} SAVED for {file}')
                except Exception as error:
                    logging.info(error)



if __name__ == "__main__":
    logging.info('Running test data, please stand by!')
    Demo.do()
