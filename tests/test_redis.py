from mjconsort.message import WithRedis
from mjconsort.objtypes import BinaryLogEvent

def test_redis_connection():
    conn = WithRedis()
    assert conn.redis.client()


def test_redis_send():
    conn = WithRedis()
    event = BinaryLogEvent()
    response = conn.send(event)
    assert response