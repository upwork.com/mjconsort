from mjconsort import utils


def test_utils_event_name():
    event_type = 23
    event_name = utils.event_name(event_type)
    assert event_name == 'WriteEvent'
