import pytest

from mjconsort.schema.conn import DB_CONN


def test_connect():
    """Connect using credentials supplied."""
    connection = DB_CONN._connect()
    assert 'MariaDB' in connection.get_server_info()