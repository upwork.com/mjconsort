from mjconsort.schema.position import Position
from mjconsort.producer.position import LogPosition

def test_last_position():
    last_position = Position.last_position()
    assert isinstance(last_position, (int, None.__class__))


def test_last_file():
    last_file = Position.last_file()
    assert isinstance(last_file, (str, None.__class__))


def test_LogPosition_get_pos():
    position = LogPosition().get_pos()
    assert isinstance(position, (int, None.__class__))


def test_LogPosition_get_file():
    binlogfile = LogPosition().get_file()
    assert isinstance(binlogfile, (str, None.__class__))
