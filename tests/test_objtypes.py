import pytest

from mjconsort import objtypes


def test_BinaryLogEvent_objtype():
    event = objtypes.BinaryLogEvent()
    assert isinstance(event.event_type, int)


def test_WriteEvent_objtype():
    event = objtypes.BinaryLogEvent(event_type=23)
    assert isinstance(event.event_type, int)


def test_DeleteEvent_objtype():
    event = objtypes.BinaryLogEvent(event_type=25)
    assert isinstance(event.event_type, int)


def test_UpdateEvent_objtype():
    event = objtypes.BinaryLogEvent(event_type=31)
    assert isinstance(event.event_type, int)
