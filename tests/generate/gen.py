import random
import uuid
from time import sleep
from types import FunctionType
from typing import NamedTuple
from collections import namedtuple
import datetime
from faker import Faker
from peewee import ModelBase, IntegrityError
import logging
import datetime
from dataclasses import dataclass, asdict

from tests.generate.ObjTypes import TVisit
from tests.generate.ObjTypes import TPatient
from tests.generate.ObjTypes import TUser
from tests.generate.ObjTypes import TPParameters
from tests.generate.ObjTypes import TPMeasurements
from tests.generate.ObjTypes import TPExaminations
from tests.generate.ObjTypes import TPSurgeries
from tests.generate.ObjTypes import TPMInformation
from tests.generate.ObjTypes import TPMInsurances
from tests.generate.ObjTypes import TPHCards
from tests.generate.ObjTypes import TPHabits
from tests.generate.ObjTypes import TPFDetails
from tests.generate.ObjTypes import TPathology
from tests.generate.ObjTypes import TObservations
from tests.generate.ObjTypes import TNStationData
from tests.generate.ObjTypes import TMetabolites
from tests.generate.ObjTypes import TMData
from tests.generate.ObjTypes import THospital
from tests.generate.ObjTypes import THParameters
from tests.generate.ObjTypes import TElectrolytes
from tests.generate.ObjTypes import TDevice
from tests.generate.ObjTypes import TCParameters
from tests.generate.ObjTypes import TUserGroups
from tests.generate.ObjTypes import TPharmacies
from tests.generate.ObjTypes import TMeasurementTypes
from tests.generate.ObjTypes import TFlywaySchemaHistory

logging.basicConfig(level=logging.INFO)
fake = Faker()

#.1 hospital
#.2 devices
#.3 usergroups
#.4 users
#.5 patients

# patient_family_details


def a_patient():
    """Generate patient data"""

    notes = [
        'Pallor and flushing', 'Symptoms involving skin tissue',
        'Loss of voice, voice disturbances ', 'Malaise and fatigue',
        'Inflammation,swelling—general', 'Convulsions, seizures '
    ]
    hereditary = [
        'Sickle Cell Anaemia', 'Hemophilia', 'Huntington Disease',
        'Muscular Dystrophy', 'Cystic Fibrosis'
    ]

    allergies = [
        'Pet Allergies', 'Drug Allergies', 'Latex Allergies',
        'Pollen Allergies'
    ]

    first_name = fake.first_name()
    last_name = fake.last_name()
    full_name = f'{first_name} {last_name}'

    return TPatient(
        uuid=fake.uuid4(),
        aadhaarId=fake.random_int(min=397788000000, max=397788999999, step=1),
        firstName=first_name,
        lastName=last_name,
        name=full_name,
        birthDate=fake.date_of_birth(minimum_age=18, maximum_age=93),
        age=random.randint(20, 89),
        sex=random.choice(['M', 'F']),
        address=fake.address(),
        sipNumber=None,
        city=fake.city(),
        email=fake.email(),
        telephone=fake.phone_number(),
        creationTime=fake.past_datetime(),
        createdByUserUUID=None,
        active=random.choice([0, 1]),
        profession=fake.job(),
        martialStatus=random.choice(['Single', 'Married', 'Divorced']),
        area=None,
        country=fake.country(),
        biometricUUID=None,
        photoUUID='e4f4b0a6-2dc2-4e2e-873f-ee02cdf69ac7',
        hospitalUUID=None,
        deviceUUID=None
    )


def a_visit():
    notes = [
        'Overexertion in lifting, lowering',
        'Direct exposure to electricity',
        'Damage to prosthetic and orthopedic devices',
        'Damage to medical implants'
    ]
    symptoms = [
        'Loss of consciousness', ' Convulsions, seizures',
        'Malaise and fatigue', 'Spasms or tremor',
        'Multiple symptoms involving nervous and musculoskeletal systems ',
        'Loss of voice, voice disturbances', 'Hyperventilation',
        'Hemoptysis (cough with hemorrhage)'
    ]
    diagnosis = [
        'Tuberculosis', 'Brucellosis', 'Helminthiases', 'Mycoses',
        'Hemorrhagic fever', 'West Nile virus'
    ]
    advice = ['Loose weight', 'Quit smoking', 'Excercise', 'Use supplements']
    medication = [
        'Levothyroxine', 'Lisinopril', 'Atorvastatin', 'Metformin', 'Amlodipine',
        'Metoprolol', 'Omeprazole', 'Simvastatin', 'Losartan', 'Albuterol',
        'Acetaminophen', 'Amoxicillin', 'Alprazolam', 'Tenormin', 'Montelukast',
        'Pantoprazole', 'Carvedilol'
    ]
    return TVisit(
        uuid=fake.uuid4(),
        patientUUID=fake.uuid4(),
        date=fake.past_datetime(),
        note=random.choice(notes),
        sms=random.choice([0, 1]),
        createdBy=fake.name(),
        createdDate=fake.past_datetime(),
        active=random.choice([0, 1]),
        type='H',
        main_complaint_symptom=random.choice(symptoms),
        main_complaint_diagnosis1=random.choice(diagnosis),
        main_complaint_diagnosis2=None,
        main_complaint_diagnosis3=None,
        secondary_complaint_diagnosis=None,
        timestamp=datetime.datetime.now(),
        examinations_prescribed=None,
        advisory=random.choice(advice),
        next_visit_date=fake.future_date(end_date='+30d'),
        comments=None,
        medications_prescribed=random.choice(medication),
        radiology_prescribed=None
    )


def a_user():
    education = ['Bsc', 'Msc', 'Phd']
    first_name = fake.first_name()
    last_name = fake.last_name()
    full_name = f'{first_name} {last_name}'

    return TUser(
        uuid=fake.uuid4(),
        usergroup=1,
        username=fake.first_name(),
        password=fake.password(),
        description=None,
        creationTime=fake.past_datetime(),
        active=1,
        email=fake.email(),
        age=random.randint(20, 89),
        birthDate=fake.date_of_birth(minimum_age=18, maximum_age=93),
        sex=random.choice(['M', 'F']),
        address=fake.address(),
        city=fake.city(),
        telephone=fake.phone_number(),
        aadhaarId=fake.random_int(min=397788000000, max=397788999999, step=1),
        education=random.choice(education),
        photoUUID='e4f4b0a6-2dc2-4e2e-873f-ee02cdf69ac7',
        firstName=first_name,
        lastName=last_name,
        name=full_name,
        providerNumber=None,
        role='admin',
        biometricUUID=None,
        voIP_number=None,
        hospitalUUID=None,
        deviceUUID=None,
        prefferedPathologies=None,
        prefferedPharmacies=None,
        prefferedRadiologies=None,
        centerId=1
    )


def a_device():
    hospitals = [
        'Mayo Clinic', 'Cleveland Clinic', 'Johns Hopkins Hospital',
        'UCLA Medical Centre', 'NewYork-Presbyterian Hospital',
        'Massachusetts General Hospital', 'Cedars-Sinai Medical Center'
    ]
    return TDevice(
        uuid=fake.uuid4(),
        machineId=None,
        hospitalUUID=None,
    )

def a_pparameter():
    """physiological_parameters"""
    return TPParameters(
        uuid=fake.uuid4(),
        respiratory_rate=fake.random_int(min=8, max=16, step=1),
        o2_saturation=fake.random_int(min=88, max=92, step=1),
        blood_pressure_sys=fake.random_int(min=50, max=145, step=1),
        blood_pressure_dia=fake.random_int(min=50, max=145, step=1),
        heart_rate=fake.random_int(min=60, max=100, step=1),
        temperature=fake.random_int(min=36, max=38, step=1),
        date=fake.past_datetime(),
        patientUUID=None
    )


def a_pmeasurement():
    """physical_measurements"""
    return TPMeasurements(
        uuid=fake.uuid4(),
        height=fake.pyfloat(right_digits=1, left_digits=2, min_value=1, max_value=2),
        weight=fake.random_int(min=125, max=240, step=1),
        date=fake.past_datetime(),
        patientUUID=None
    )


def a_pexamination():
    """photo_examinations"""
    scantypes = ['MRI', 'X-RAY', 'CT/CAT', 'Ultrasound']
    descriptions = [
        'An ultrasound uses high-frequency sound',
        'A CT scan is sort of our workhorse of radiolog', 
        'MRIs are very commonly used for brain imaging'
    ]
    return TPExaminations(
        uuid=fake.uuid4(),
        description=random.choice(descriptions),
        type=random.choice(scantypes),
        photo=None,
        date=fake.past_datetime(),
        patientUUID=None
    )


def a_psurgery():
    """patient_surgeries"""
    reasons = [
        'Arthroscopy', 'Breast Biopsy', 'Circumcision', 'Caesarean Section'
    ]

    return TPSurgeries(
        uuid=fake.uuid4(),
        name=fake.name(),
        year=fake.year(),
        reason=random.choice(reasons),
        doctor=fake.name(),
        patientUUID=None
    )


def a_pminformation():
    """patient_medical_information"""
    medical_conditions = [
        'Diabetes', 'COPD', 'Cancer', 'Sleep Apnea', 'Depression',
        'Obesity', 'Asthma'
    ]

    nature_medical_condition = [
        'Carpal tunnel syndrome', 'Tarsal tunnel syndrome', 'Glaucoma '
        'Pneumonia', 'Berylliosis', 'Helminthiases'
    ]

    allergies = [
        'Pet Allergies', 'Drug Allergies', 'Latex Allergies',
        'Pollen Allergies'
    ]

    return TPMInformation(
        uuid=fake.uuid4(),
        bloodType=random.choice(['A', 'AB', 'B', 'O']),
        previousMedicalConditions=random.choice(medical_conditions),
        natureOfMedicalCondition=random.choice(nature_medical_condition),
        existingMedication=random.choice(medical_conditions),
        allergies=random.choice(allergies),
        patientUUID=None
    )

def a_pinsurance():
    """patient_insurances"""
    return TPMInsurances(
        uuid=fake.uuid4(),
        company=fake.company(),
        number=fake.phone_number(),
        patientUUID=None
    )


def a_phcards():
    """patient_health_cards"""
    return TPHCards(
        uuid=fake.uuid4(),
        type=random.choice([0, 1]),
        number=fake.phone_number(),
        patientUUID=None
    )


def a_phabit():
    """patient_habits"""
    habits = ['smoking', 'drinking']
    return TPHabits(
        uuid=fake.uuid4(),
        habitName=random.choice(habits),
        habitYears=None,
        habitFrequency=None,
        patientUUID=None
    )


def a_pfdetails():
    """patient_family_details"""
    hereditary = [
        'Sickle Cell Anaemia', 'Hemophilia', 'Huntington Disease',
        'Muscular Dystrophy', 'Cystic Fibrosis'
    ]

    return TPFDetails(
        uuid=fake.uuid4(),
        motherName=fake.first_name(),
        motherStatus=random.choice([0, 1]),
        fatherName=fake.last_name(),
        fatherStatus=random.choice([0, 1]),
        parentsTogether=random.choice([0, 1]),
        nextOfKin=fake.name(),
        hereditaryDiseases=random.choice(hereditary),
        nextOfKinRelationShip=random.choice(['Wife', 'Parent', 'Sibling']),
        nextOfKinTelephone=fake.phone_number(),
        patientUUID=None
    )


def a_pathology():
    """pathology"""
    return TPathology(
        uuid=fake.uuid4(),
        date=fake.past_datetime(),
        data=None,
        patientUUID=None
    )

def a_observation():
    """observations"""
    return TObservations(
        uuid=fake.uuid4(),
        taste=fake.random_int(min=1, max=7, step=1),
        smell=fake.random_int(min=19, max=40, step=1),
        consciousness=fake.random_int(min=8, max=13, step=1),
        date=fake.past_datetime(),
        patientUUID=None
    )

def a_nstationdata():
    """nursing_station_data"""
    return TNStationData(
        uuid=fake.uuid4(),
        patientUUID=None,
        deviceUUID=None,
        heart_rate=None,
        o2_saturation=fake.random_int(min=88, max=92, step=1),
        blood_pressure_sys=fake.random_int(min=50, max=145, step=1),
        blood_pressure_dia=fake.random_int(min=50, max=145, step=1),
        datetime=fake.past_datetime(),
        past_tasks=None,
        future_tasks=None,
        pathology=None,
        blood_glucose=fake.random_int(min=140, max=199, step=1),
        temperature=fake.random_int(min=36, max=38, step=1),
        drip_flow=0,
        oxygen_flow=fake.random_int(min=20, max=60, step=1),
    )


def a_metabolites():
    """metabolites"""
    return TMetabolites(
        uuid=fake.uuid4(),
        glu=None,
        lac=None,
        date=fake.past_datetime(),
        patientUUID=None
    )


def a_mdata():
    """measurements_data"""
    return TMData(
        uuid=fake.uuid4(),
        type=random.choice([0, 1]),
        vale=None,
        date=fake.past_datetime(),
        patientUUID=None,
    )

def a_hospital():
    """hospitals"""
    hospitals = [
        'Mayo Clinic', 'Cleveland Clinic', 'Johns Hopkins Hospital',
        'UCLA Medical Centre', 'NewYork-Presbyterian Hospital',
        'Massachusetts General Hospital', 'Cedars-Sinai Medical Center'
    ]

    return THospital(
        uuid=fake.uuid4(),
        name=random.choice(hospitals),
        branch=random.choice(['Broad Str', '34th Str', 'Albany']),
        ward=random.choice(['Cardiology', 'Oncology', 'ICU', 'OBGYN']),
        address=fake.address(),
        email=fake.email(),
        password=fake.password(),
        telephone=fake.phone_number(),
        websiteAddress=fake.url(),
        haveBranches=1,
        numberOfUsers=None,
        erp_token=None,
        hospitalAdminName=fake.name(),
        hospitalAdminEmail=fake.email()
    )


def a_hparameter():
    """hematological_parameters"""
    return THParameters(
        uuid=fake.uuid4(),
        blood_glucose=None,
        hemoglobin=None,
        blood_gas=None,
        pH=None,
        pcO2=None,
        po2=None,
        hct=None,
        date=fake.past_datetime(),
        patientUUID=None,
    )


def a_electrolytes():
    """electrolytes"""
    return TElectrolytes(
        uuid=fake.uuid4(),
        na=None,
        k=None,
        iCa=None,
        Li=None,
        pH=None,
        Cl=None,
        date=fake.past_datetime(),
        patientUUID=None
    )


def a_cparameter():
    """calculated_parameters"""
    return TCParameters(
        uuid=fake.uuid4(),
        Hb=None,
        hcO3=None,
        be=None,
        be_b=None,
        be_ecf=None,
        tcO2=None,
        ag=None,
        O2sat=None,
        O2Ct=None,
        sbs=None,
        date=fake.past_datetime(),
        patientUUID=None
    )


def a_usergroup():
    """usergroups"""
    ug = namedtuple('usergroup', ['name', 'description'])
    ugroups = [
        ug('admin', 'User with all he privileges'),
        ug('doctor', 'Doctor is responsible to check patients and test'),
        ug('guest', 'read only users'),
        ug('pahologist', 'Pathology is responsible to upload patients report')
    ]
    u = random.choice(ugroups)
    return TUserGroups(
        uuid=fake.uuid4(),
        name=u.name,
        description=u.description
    )


def a_pharmacy():
    """pharmacy"""
    return TPharmacies(
        uuid=fake.uuid4(),
        email=fake.email(),
        address=fake.address(),
        city=fake.city(),
        telephone=fake.phone_number(),
        name=fake.name()
    )

def a_flywayschemahistory():
    """flyway_schema_history"""
    return TFlywaySchemaHistory(
        installed_rank=None,
        version=None,
        description=None,
        type=None,
        script=None,
        checksum=None,
        installed_by=None,
        installed_on=None,
        execution_time=None,
        success=random.choice([0, 1])
    )


def a_measurementtype():
    return TMeasurementTypes(
        name='height',
        description='how tall'
    )
class GenerateData:
    @staticmethod
    def gen():
        """Generate and insert data from tables."""

        device_ = a_device()
        device_details.insert(**device_.asdict()).execute()

        while True:
            try:

                patient_ = a_patient()
                patient_.PAT_MACHINE_ID = device_.MachineID
                patient.insert(**asdict(patient_)).execute()

                examination_ = an_examination()
                examination_.EXAMINATION_PAT_ID = patient_.PAT_AADHAAR_ID
                examination.insert(**asdict(examination_)).execute()
                e_id = examination.get(
                    examination.EXAMINATIONS_UUID == examination_.EXAMINATIONS_UUID
                ).id

                visit_ = a_visit()
                visit_.VST_PAT_EXAM = examination_.EXAMINATIONS_UUID
                visit_.VST_PAT_ID = patient_.PAT_AADHAAR_ID
                visits.insert(**asdict(visit_)).execute()

                logging.info(f'patient added.{random.randint(10, 10000)}')
            except IntegrityError:
                logging.info('This is a duplicate, skipping to proceed!')


    @staticmethod
    def delete():
        """Random delete"""
        tables = [device_details, user, different_centre]
        table = random.choice(tables)
        record = table.delete().limit(1).execute()
