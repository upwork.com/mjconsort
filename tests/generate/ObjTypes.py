import typing
import peewee
import uuid
import dataclasses


@dataclasses.dataclass
class TPatient:
    uuid: peewee.CharField = None
    aadhaarId: peewee.CharField = None
    firstName: peewee.CharField = None
    lastName: peewee.CharField = None
    name: peewee.CharField = None
    birthDate: peewee.CharField = None
    age: peewee.CharField = None
    sex: peewee.CharField = None
    address: peewee.CharField = None
    sipNumber: peewee.CharField = None
    city: peewee.CharField = None
    email: peewee.CharField = None
    telephone: peewee.CharField = None
    creationTime: peewee.CharField = None
    createdByUserUUID: peewee.CharField = None
    active: peewee.CharField = None
    profession: peewee.CharField = None
    martialStatus: peewee.CharField = None
    area: peewee.CharField = None
    country: peewee.CharField = None
    biometricUUID: peewee.CharField = None
    photoUUID: peewee.CharField = None
    hospitalUUID: peewee.CharField = None
    deviceUUID: peewee.CharField = None

    def asdict(self):
        return dataclasses.asdict(self)


@dataclasses.dataclass
class TUser:
    uuid: peewee.CharField = None
    usergroup: peewee.CharField = None
    username: peewee.CharField = None
    password: peewee.CharField = None
    description: peewee.CharField = None
    creationTime: peewee.CharField = None
    active: peewee.CharField = None
    email: peewee.CharField = None
    age: peewee.CharField = None
    birthDate: peewee.CharField = None
    sex: peewee.CharField = None
    address: peewee.CharField = None
    city: peewee.CharField = None
    telephone: peewee.CharField = None
    aadhaarId: peewee.CharField = None
    education: peewee.CharField = None
    photoUUID: peewee.CharField = None
    firstName: peewee.CharField = None
    lastName: peewee.CharField = None
    name: peewee.CharField = None
    providerNumber: peewee.CharField = None
    role: peewee.CharField = None
    biometricUUID: peewee.CharField = None
    voIP_number: peewee.CharField = None
    hospitalUUID: peewee.CharField = None
    deviceUUID: peewee.CharField = None
    prefferedPathologies: peewee.CharField = None
    prefferedPharmacies: peewee.CharField = None
    prefferedRadiologies: peewee.CharField = None
    centerId: peewee.CharField = None

    def asdict(self):
        return dataclasses.asdict(self)


@dataclasses.dataclass
class TVisit:
    uuid: peewee.UUIDField = None
    patientUUID: peewee.UUIDField = None
    date: peewee.CharField = None
    note: peewee.CharField = None
    sms: peewee.CharField = None
    createdBy: peewee.CharField = None
    createdDate: peewee.DateTimeField = None
    active: peewee.CharField = None
    type: peewee.CharField = None
    main_complaint_symptom: peewee.CharField = None
    main_complaint_diagnosis1: peewee.CharField = None
    main_complaint_diagnosis2: peewee.CharField = None
    main_complaint_diagnosis3: peewee.CharField = None
    secondary_complaint_diagnosis: peewee.CharField = None
    timestamp: peewee.TimestampField = None
    examinations_prescribed: peewee.CharField = None
    advisory: peewee.CharField = None
    next_visit_date: peewee.DateTimeField = None
    comments: peewee.CharField = None
    medications_prescribed: peewee.CharField = None
    radiology_prescribed: peewee.CharField = None

    def asdict(self):
        return dataclasses.asdict(self)


@dataclasses.dataclass
class TPParameters:
    uuid: peewee.CharField = None
    respiratory_rate: peewee.CharField = None
    o2_saturation: peewee.CharField = None
    blood_pressure_sys: peewee.CharField = None
    blood_pressure_dia: peewee.CharField = None
    heart_rate: peewee.CharField = None
    temperature: peewee.CharField = None
    date: peewee.CharField = None
    patientUUID: peewee.CharField = None

    def asdict(self):
        return dataclasses.asdict(self)


@dataclasses.dataclass
class TPMeasurements:
    uuid: peewee.CharField = None
    height: peewee.CharField = None
    weight: peewee.CharField = None
    date: peewee.CharField = None
    patientUUID: peewee.CharField = None

    def asdict(self):
        return dataclasses.asdict(self)


@dataclasses.dataclass
class TPExaminations:
    uuid: peewee.CharField = None
    description: peewee.CharField = None
    type: peewee.CharField = None
    photo: peewee.CharField = None
    date: peewee.CharField = None
    patientUUID: peewee.CharField = None

    def asdict(self):
        return dataclasses.asdict(self)


@dataclasses.dataclass
class TPSurgeries:
    uuid: peewee.CharField = None
    name: peewee.CharField = None
    year: peewee.CharField = None
    reason: peewee.CharField = None
    doctor: peewee.CharField = None
    patientUUID: peewee.CharField = None

    def asdict(self):
        return dataclasses.asdict(self)


@dataclasses.dataclass
class TPMInformation:
    uuid: peewee.CharField = None
    bloodType: peewee.CharField = None
    previousMedicalConditions: peewee.CharField = None
    natureOfMedicalCondition: peewee.CharField = None
    existingMedication: peewee.CharField = None
    allergies: peewee.CharField = None
    patientUUID: peewee.CharField = None

    def asdict(self):
        return dataclasses.asdict(self)


@dataclasses.dataclass
class TPMInsurances:
    uuid: peewee.CharField = None
    company: peewee.CharField = None
    number: peewee.CharField = None
    patientUUID: peewee.CharField = None

    def asdict(self):
        return dataclasses.asdict(self)


@dataclasses.dataclass
class TPHCards:
    uuid: peewee.CharField = None
    type: peewee.CharField = None
    number: peewee.CharField = None
    patientUUID: peewee.CharField = None

    def asdict(self):
        return dataclasses.asdict(self)


@dataclasses.dataclass
class TPHabits:
    uuid: peewee.CharField = None
    habitName: peewee.CharField = None
    habitYears: peewee.CharField = None
    habitFrequency: peewee.CharField = None
    patientUUID: peewee.CharField = None

    def asdict(self):
        return dataclasses.asdict(self)


@dataclasses.dataclass
class TPFDetails:
    uuid: peewee.CharField = None
    motherName: peewee.CharField = None
    motherStatus: peewee.CharField = None
    fatherName: peewee.CharField = None
    fatherStatus: peewee.CharField = None
    parentsTogether: peewee.CharField = None
    nextOfKin: peewee.CharField = None
    hereditaryDiseases: peewee.CharField = None
    nextOfKinRelationShip: peewee.CharField = None
    nextOfKinTelephone: peewee.CharField = None
    patientUUID: peewee.CharField = None

    def asdict(self):
        return dataclasses.asdict(self)


@dataclasses.dataclass
class TPathology:
    uuid: peewee.CharField = None
    date: peewee.CharField = None
    data: peewee.BlobField = None
    patientUUID: peewee.CharField = None

    def asdict(self):
        return dataclasses.asdict(self)


@dataclasses.dataclass
class TObservations:
    uuid: peewee.CharField = None
    taste: peewee.CharField = None
    smell: peewee.CharField = None
    consciousness: peewee.CharField = None
    date: peewee.DateTimeField = None
    patientUUID: peewee.CharField = None

    def asdict(self):
        return dataclasses.asdict(self)


@dataclasses.dataclass
class TNStationData:
    uuid: peewee.CharField = None
    patientUUID: peewee.CharField = None
    deviceUUID: peewee.CharField = None
    heart_rate: peewee.CharField = None
    o2_saturation: peewee.CharField = None
    blood_pressure_sys: peewee.CharField = None
    blood_pressure_dia: peewee.CharField = None
    datetime: peewee.CharField = None
    past_tasks: peewee.CharField = None
    future_tasks: peewee.CharField = None
    pathology: peewee.CharField = None
    blood_glucose: peewee.CharField = None
    temperature: peewee.CharField = None
    drip_flow: peewee.CharField = None
    oxygen_flow: peewee.CharField = None

    def asdict(self):
        return dataclasses.asdict(self)


@dataclasses.dataclass
class TMetabolites:
    uuid: peewee.CharField = None
    glu: peewee.CharField = None
    lac: peewee.CharField = None
    date: peewee.CharField = None
    patientUUID: peewee.CharField = None

    def asdict(self):
        return dataclasses.asdict(self)


@dataclasses.dataclass
class TMData:
    uuid: peewee.CharField = None
    type: peewee.CharField = None
    vale: peewee.CharField = None
    date: peewee.CharField = None
    patientUUID: peewee.CharField = None

    def asdict(self):
        return dataclasses.asdict(self)


@dataclasses.dataclass
class THospital:
    uuid: peewee.CharField = None
    name: peewee.CharField = None
    branch: peewee.CharField = None
    ward: peewee.CharField = None
    address: peewee.CharField = None
    email: peewee.CharField = None
    password: peewee.CharField = None
    telephone: peewee.CharField = None
    websiteAddress: peewee.CharField = None
    haveBranches: peewee.CharField = None
    numberOfUsers: peewee.CharField = None
    erp_token: peewee.CharField = None
    hospitalAdminName: peewee.CharField = None
    hospitalAdminEmail: peewee.CharField = None

    def asdict(self):
        return dataclasses.asdict(self)


@dataclasses.dataclass
class THParameters:
    uuid: peewee.CharField = None
    blood_glucose: peewee.CharField = None
    hemoglobin: peewee.CharField = None
    blood_gas: peewee.CharField = None
    pH: peewee.CharField = None
    pcO2: peewee.CharField = None
    po2: peewee.CharField = None
    hct: peewee.CharField = None
    date: peewee.CharField = None
    patientUUID: peewee.CharField = None

    def asdict(self):
        return dataclasses.asdict(self)


@dataclasses.dataclass
class TElectrolytes:
    uuid: peewee.CharField = None
    na: peewee.CharField = None
    k: peewee.CharField = None
    iCa: peewee.CharField = None
    Li: peewee.CharField = None
    pH: peewee.CharField = None
    Cl: peewee.CharField = None
    date: peewee.CharField = None
    patientUUID: peewee.CharField = None

    def asdict(self):
        return dataclasses.asdict(self)


@dataclasses.dataclass
class TDevice:
    uuid: peewee.CharField = None
    machineId: peewee.CharField = None
    hospitalUUID: peewee.CharField = None

    def asdict(self):
        return dataclasses.asdict(self)


@dataclasses.dataclass
class TCParameters:
    """calculated_parameters"""
    uuid: peewee.CharField = None
    Hb: peewee.CharField = None
    hcO3: peewee.CharField = None
    be: peewee.CharField = None
    be_b: peewee.CharField = None
    be_ecf: peewee.CharField = None
    tcO2: peewee.CharField = None
    ag: peewee.CharField = None
    O2sat: peewee.CharField = None
    O2Ct: peewee.CharField = None
    sbs: peewee.CharField = None
    date: peewee.CharField = None
    patientUUID: peewee.CharField = None

    def asdict(self):
        return dataclasses.asdict(self)


@dataclasses.dataclass
class TUserGroups:
    """usergroups"""
    uuid: peewee.CharField = None
    name: peewee.CharField = None
    description: peewee.CharField = None

    def asdict(self):
        return dataclasses.asdict(self)


@dataclasses.dataclass
class TPharmacies:
    """pharmacies"""
    uuid: peewee.CharField = None
    email: peewee.CharField = None
    address: peewee.CharField = None
    city: peewee.CharField = None
    telephone: peewee.CharField = None
    name: peewee.CharField = None

    def asdict(self):
        return dataclasses.asdict(self)


@dataclasses.dataclass
class TFlywaySchemaHistory:
    """flyway_schema_history"""
    installed_rank: peewee.CharField = None
    version: peewee.CharField = None
    description: peewee.CharField = None
    type: peewee.CharField = None
    script: peewee.CharField = None
    checksum: peewee.CharField = None
    installed_by: peewee.CharField = None
    installed_on: peewee.CharField = None
    execution_time: peewee.CharField = None
    success: peewee.CharField = None

    def asdict(self):
        return dataclasses.asdict(self)


@dataclasses.dataclass
class TMeasurementTypes:
    name: peewee.CharField = None
    description: peewee.CharField = None

    def asdict(self):
        return dataclasses.asdict(self)