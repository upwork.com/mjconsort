"""
TESTSCRIPT.PY
MJCONSORT/MEDJACKET

This script does not generate/create/build the data. Data is retrieve from
spreadsheets which are located in tests/data.

This script does not check for duplicate values nor missing data. If any of these
occurs the database-server (mariadb) will handle it.

See test.md for instructions.

Behaviour:
1. Insert a hospital, device, and user
2. Insert as many patients and related data in relation to (1)
"""


import argparse
import logging
import os
import random
from typing import Union, Dict
import pandas
import peewee

from mjconsort.db import models
from mjconsort.db.position import Position
from mjconsort.db.conn import DB_CONN

logging.basicConfig(level=logging.INFO)
parser = argparse.ArgumentParser()


class Demo:
    def __init__(self):
        self.files = [file.path for file in os.scandir('tests/data')]
        self.known_hospital: Union[Dict, None] = None
        self.known_device: Union[Dict, None]  = None
        self.known_user: Union[Dict, None]  = None
        self.known_user_group_uuid: Union[str, None] = None
        self.known_hospital_fn = None
        self.known_device_fn = None
        self.known_user_fn = None
        self.known_user_group_fn = lambda val: '6429b31b-d3bd-4482-af34-dd12534c6172'
        self.to_datetime_fn = lambda val: str(val) #val.to_pydatetime()
        self.random_nos = lambda val: random.choice(range(1, 7))
        self.replace_apostrophe = lambda val: val.replace("'", "")

    def do(self):
        for file in self.files:
            logging.info(f'Reading a spreadsheet with name {file}!')

            # hospital
            healthdata = pandas.ExcelFile(file)
            hospital_df_ = pandas.read_excel(healthdata, 'hospitals').fillna('')
            hospital_df = hospital_df_.rename(index=str, columns={'htps://': 'websiteAddress'})
            self.known_hospital = hospital_df.to_dict(orient='records')[0]
            try:
                models.Hospital.insert(self.known_hospital).execute()
                logging.info(f'HOSPITAL  {self.known_hospital["name"]} SAVED for {file}')
            except peewee.IntegrityError as error:
                logging.info(f'HOSPITAL, {error}')

            # update self.known_hospital_fn
            self.known_hospital_fn = lambda val: self.known_hospital['uuid']


            # devices
            healthdata = pandas.ExcelFile(file)
            devices_df = pandas.read_excel(healthdata, 'devices').fillna('')
            self.known_device = devices_df.to_dict(orient='records')[0]
            try:
                models.Devices.insert(self.known_device).execute()
                logging.info(f'DEVICE with  {self.known_device["uuid"]} SAVED for {file}')
            except peewee.IntegrityError as error:
                logging.info(f'DEVICES, {error}')

            # update self.known_device_fn
            self.known_device_fn = lambda val: self.known_device['uuid']


            # usergroups
            # Universal usergroups, see schema.

            #user
            healthdata = pandas.ExcelFile(file)
            user_df = pandas.read_excel(healthdata, 'user').fillna('')
            user_df['usergroup'] = user_df.usergroup.apply(self.known_user_group_fn)
            user_df['deviceUUID'] = user_df.deviceUUID.apply(self.known_device_fn)
            user_df['birthDate'] = user_df.birthDate.apply(self.to_datetime_fn)

            # drop column CreationTime
            user_df = user_df.drop('creationTime', axis=1)
            self.known_user = user_df.to_dict(orient='records')[0]
            try:
                models.User.insert(self.known_user).execute()
                logging.info(f'USER  {self.known_user["uuid"]} SAVED for {file}')
            except Exception as error:
                logging.info(f'USER, {error}')

            # update self.known_user_fn
            self.known_user_fn = (lambda val: self.known_user['uuid'])


            # patient
            healthdata = pandas.ExcelFile(file)
            patient_df = pandas.read_excel(healthdata, 'patients').fillna('')
            patient_df = patient_df.drop('creationTime', axis=1)
            patient_df['birthDate'] = patient_df.birthDate.apply(self.to_datetime_fn)
            patient_df['deviceUUID'] = patient_df.deviceUUID.apply(self.known_device_fn)
            patient_df['hospitalUUID'] = patient_df.hospitalUUID.apply(self.known_hospital_fn)
            patient_df['createdByUserUUID'] = patient_df.createdByUserUUID.apply(self.known_user_fn)
            patient_list = patient_df.to_dict(orient='records')
            for p in patient_list:
                try:
                    models.Patients.insert(p).execute()
                    logging.info(f'PATIENT  {p["name"]} SAVED for {file}')
                except Exception as error:
                    logging.info(error)



            # patient_family_details
            healthdata = pandas.ExcelFile(file)
            patient_df = pandas.read_excel(healthdata, 'patient_family_details').fillna('')
            patient_list = patient_df.to_dict(orient='records')
            for p in patient_list:
                try:
                    models.PatientFamilyDetails.insert(p).execute()
                    logging.info(f'Family details for PATIENT with kin {p["nextOfKin"]} SAVED for {file}')
                except Exception as error:
                    logging.info(error)


            # patient_habit
            healthdata = pandas.ExcelFile(file)
            patient_df = pandas.read_excel(healthdata, 'patient_habits').fillna('')
            patient_df['habitYears'] = patient_df.habitYears.apply(self.random_nos)
            patient_df['habitFrequency'] = patient_df.habitFrequency.apply(self.random_nos)
            patient_list = patient_df.to_dict(orient='records')
            for p in patient_list:
                try:
                    models.PatientHabits.insert(p).execute()
                    logging.info(f'HABIT for PATIENT with uuid {p["patientUUID"]} SAVED for {file}')
                except Exception as error:
                    logging.info(error)


            # patient_health_cards
            healthdata = pandas.ExcelFile(file)
            patient_df = pandas.read_excel(healthdata, 'patient_health_cards').fillna('')
            patient_list = patient_df.to_dict(orient='records')
            for p in patient_list:
                try:
                    models.PatientHealthCards.insert(p).execute()
                    logging.info(f'Patient health card for PATIENT with uuid {p["patientUUID"]} SAVED for {file}')
                except Exception as error:
                    logging.info(error)


            # patient_insurances
            healthdata = pandas.ExcelFile(file)
            patient_df = pandas.read_excel(healthdata, 'patient_insurances').fillna('')
            patient_list = patient_df.to_dict(orient='records')
            for p in patient_list:
                try:
                    models.PatientInsurances.insert(p).execute()
                    logging.info(f'Patient insurance for PATIENT with uuid {p["patientUUID"]} SAVED for {file}')
                except Exception as error:
                    logging.info(error)


            #patient surgeries
            healthdata = pandas.ExcelFile(file)
            patient_df = pandas.read_excel(healthdata, 'patient_surgeries').fillna('')
            patient_df['year'] = patient_df.year.apply(self.replace_apostrophe)
            patient_list = patient_df.to_dict(orient='records')
            for p in patient_list:
                try:
                    models.PatientSurgeries.insert(p).execute()
                    logging.info(f'Patient surgery for PATIENT with uuid {p["patientUUID"]} SAVED for {file}')
                except Exception as error:
                    logging.info(error)


class Utils:
    @staticmethod
    def flush():
        """Clears respective tables and binlog"""
        # DB_CONN.execute_sql('SET FOREIGN_KEY_CHECKS=0;')
        DB_CONN.execute_sql('DELETE from hospital WHERE uuid not like "59b254ff-7ba6-4c1a-82c8-356fe145dea6";')
        DB_CONN.execute_sql('DELETE from devices WHERE uuid not like "de542181-5960-4cea-b423-e5ed5decf541";')
        DB_CONN.execute_sql('DELETE from user WHERE uuid not like "2bcf41d7-849e-47c2-9361-845bebc621d4";')
        Position.truncate_table()

    @staticmethod
    def basicchecks():
        hospital_cnt = DB_CONN.execute_sql('SELECT COUNT(*) FROM hospital;').fetchone()[0]
        user_cnt = DB_CONN.execute_sql('SELECT COUNT(*) FROM user;').fetchone()[0]
        devices_cnt = DB_CONN.execute_sql('SELECT COUNT(*) FROM devices;').fetchone()[0]
        if hospital_cnt > 2 or user_cnt > 2 or devices_cnt > 2:
            logging.info('Please use --flush before --runtest')
            exit()


if __name__ == "__main__":
    parser.add_argument("--flush", action="store_true", help="truncates table respective tables and binlog")
    parser.add_argument("--runtest", action="store_true", help="start inserting data into the db")

    args = parser.parse_args()
    if args.flush:
        Utils.flush()
    if args.runtest:
        Utils.basicchecks()
        Demo().do()
