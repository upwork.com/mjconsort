This document provides instructions for running tests using  testcript_basic.py on Linux and
Windows computers. Start with the checklist before proceeding.


# Config/Credentials
Credentials are passed as environment variables using 'export'.  
These should be exported on consumers and producers.  
Producers should have the consumer IP as REDIS_HOST.

Note: testscript_basic.py, start_consumer.py and start_producer.py require env vars.

### Producer env vars
export DB_NAME=  
export DB_USER=  
export DB_PASS=  
export DB_HOST=  
export REDIS_HOST=[CONSUMER-IP]

### Consumer env vars
export DB_NAME=  
export DB_USER=  
export DB_PASS=  
export DB_HOST=  
export REDIS_HOST=[CONSUMER-IP]


# Checklist
- Mariadb/Mysql
- Python >= 3.7
  

If the above is not available on your system please install before proceeding.

1. For Linux Raspberry
    - sudo apt install mariadb python3.7 python3-venv python3-pip libatlas-base-dev -y
2. For Linux Debian/Ubuntu (non Raspberry)
    - sudo apt install mariadb python3.7 python3-venv python3-pip redis -y
3. For Windows
    - Mariadb ; https://downloads.mariadb.org/
    - Python ; https://www.python.org/downloads/release/python-377/


## Enable BinaryLogging & Replication on Mariadb
1. Find the my.ini or my.cnf and add the following to the bottom of file;  
[mysqld]  
server-id=1  
binlog_format=row   
log_bin=binlog  

For Linux: Go to /etc/mysql/my.cnf 

For Windows;
1. This article explians how to find the my.cnf or my.ini file  
https://blog.sqlauthority.com/2014/01/07/mysql-locate-the-configuration-file-my-ini-or-my-cnf-on-windows-platform/

2. Log into MySQL and run;  
    - GRANT REPLICATION SLAVE, REPLICATION CLIENT ON *.* TO [user-name]@localhost identified by [user-password];
3. Restart the MySQL


## Install Python packages on producer and consumer

For Linux: 
1. Setup the virtual environment for Python
    - python3 -m venv mjconsort-env 
2. Enable the environment
    - source mjconsort-env/bin/activate
3. Install the packages
    - pip3 install -r requirements.txt


## Running the test

IMPORTANT: Make sure you have the /testdata directory in the same directory as the
testscript.py. So, you should have testscript.py and testdata dir (contains spreadsheets)

IMPORTANT: This test is for replication, as such the spreadsheets should be spread
amongs the machines. Each machine should have a unique or a number of unique spreadsheets.
Having spreadsheet 1 in Machine 1 as well as Machine 2 will result in an integrity error.
So, for instance; if we have spreadsheets number 1 - 30 and 3 machines , we want
the 30 spreadsheet split amongst the 3 machines. They should be placed in the testdata folder.

## PRODUCER 
For Linux:
1. Make sure the virtual environment is enabled, you are going to run 2 files.
    - testscript.py : Reads spreadsheets and insert into a corresponding table.
    - start_producer.py : Reads binlogs and sends to master db via Redis

2. You can use tmux or screen to divide the screen for ease.
    - python start_producer.py
    - python testscript.py


## CONSUMER
1. Make sure the virtual environment is enabled, you are going to run 1 files.
    - start_consumer.py : Reads binlogs and sends to master db via Redis

2. Edit the redis.conf at /etc/redis/redis.conf
    - disable protected mode
    - comment out bind 127.0.0.1
    - to connect to redis and flush the db run
        - redis-cli
        - select 0
        - flushdb


# IMPORTANT COMMANDS AND NOTES
-   Reset the MySQL master log on the producer when starting or restarting a test using - RESET MASTER;
-   You can look at the master status using - SHOW MASTER STATUS;
-   Clear the position everytime you reset the master log in MySQL;
    - python3.7 (start the python interpreter)
    - from mjconsort.schema.position import Position
    - Position.truncate_table()
