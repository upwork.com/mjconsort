-- MySQL dump 10.13  Distrib 5.7.33, for Linux (x86_64)
--
-- Host: localhost    Database: medjacketdb
-- ------------------------------------------------------
-- Server version	5.7.33-0ubuntu0.18.04.1-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


--
-- Table structure for table `hospital`
--

DROP TABLE IF EXISTS `hospital`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hospital` (
  `uuid` varchar(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `branch` varchar(255) DEFAULT NULL,
  `ward` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `websiteAddress` varchar(255) DEFAULT NULL,
  `haveBranches` varchar(255) DEFAULT NULL,
  `numberOfUsers` varchar(255) DEFAULT NULL,
  `erp_token` varchar(255) DEFAULT NULL,
  `hospitalAdminName` varchar(255) DEFAULT NULL,
  `hospitalAdminEmail` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hospital`
--

LOCK TABLES `hospital` WRITE;
/*!40000 ALTER TABLE `hospital` DISABLE KEYS */;
INSERT INTO `hospital` VALUES ('59b254ff-7ba6-4c1a-82c8-356fe145dea6','St George Hospital','Branch 1','1','Fifth road','stgeorgehospital@mail.com','$2a$10$umVnUpecSoZPFH.UbXI0g.x1LuEEZgK61KQ4zTM05rSGh2bZL3vPS','+11111111111','stgeorgehospital.com','M','2','erpToken1','Nick James','drnickjames@mail.com');
/*!40000 ALTER TABLE `hospital` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `devices`
--

DROP TABLE IF EXISTS `devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devices` (
  `uuid` varchar(36) NOT NULL,
  `machineId` varchar(255) DEFAULT NULL,
  `hospitalUUID` varchar(36) NOT NULL,
  PRIMARY KEY (`uuid`),
  UNIQUE KEY `machineId` (`machineId`),
  KEY `dd_hos_id_key` (`hospitalUUID`),
  CONSTRAINT `dd_hos_id_fk` FOREIGN KEY (`hospitalUUID`) REFERENCES `hospital` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `devices`
--

LOCK TABLES `devices` WRITE;
/*!40000 ALTER TABLE `devices` DISABLE KEYS */;
INSERT INTO `devices` VALUES ('de542181-5960-4cea-b423-e5ed5decf541','F8-FF-C2-11-26-77','59b254ff-7ba6-4c1a-82c8-356fe145dea6');
/*!40000 ALTER TABLE `devices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patients`
--

DROP TABLE IF EXISTS `patients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patients` (
  `uuid` varchar(36) NOT NULL,
  `aadhaarId` varchar(255) NOT NULL,
  `firstName` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `birthDate` datetime DEFAULT NULL,
  `age` int(11) NOT NULL DEFAULT '0',
  `sex` varchar(4) NOT NULL DEFAULT ' ',
  `address` varchar(255) DEFAULT NULL,
  `sipNumber` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `creationTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `createdByUserUUID` varchar(36) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `profession` varchar(255) NOT NULL DEFAULT 'unknown',
  `martialStatus` varchar(255) NOT NULL DEFAULT 'unknown',
  `area` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `biometricUUID` varchar(36) DEFAULT NULL,
  `photoUUID` varchar(36) DEFAULT 'e4f4b0a6-2dc2-4e2e-873f-ee02cdf69ac7',
  `hospitalUUID` varchar(36) NOT NULL,
  `deviceUUID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`uuid`),
  UNIQUE KEY `aadhaarId` (`aadhaarId`),
  KEY `p_user_key` (`createdByUserUUID`),
  KEY `p_hospital_key` (`hospitalUUID`),
  KEY `p_device_key` (`deviceUUID`),
  CONSTRAINT `p_device_fk` FOREIGN KEY (`deviceUUID`) REFERENCES `devices` (`uuid`) ON DELETE CASCADE,
  CONSTRAINT `p_hospital_fk` FOREIGN KEY (`hospitalUUID`) REFERENCES `hospital` (`uuid`) ON DELETE CASCADE,
  CONSTRAINT `p_user_fk` FOREIGN KEY (`createdByUserUUID`) REFERENCES `user` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patients`
--

LOCK TABLES `patients` WRITE;
/*!40000 ALTER TABLE `patients` DISABLE KEYS */;
INSERT INTO `patients` VALUES ('1e83137e-347e-4b8f-a414-66a7dc466064','993883885778','Andrew','Robertson','Andrew Robertson','1996-05-01 00:00:00',24,'M','Fifth Ave','1234','Mumbai','andrewrobertson@gmail.com','34563463456','2020-11-04 00:56:46','2bcf41d7-849e-47c2-9361-845bebc621d4',1,'Engineer','unmarried','Brundy','India',NULL,'e4f4b0a6-2dc2-4e2e-873f-ee02cdf69ac7','59b254ff-7ba6-4c1a-82c8-356fe145dea6','de542181-5960-4cea-b423-e5ed5decf541');
/*!40000 ALTER TABLE `patients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `electrolytes`
--

DROP TABLE IF EXISTS `electrolytes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `electrolytes` (
  `uuid` varchar(36) NOT NULL,
  `na` double DEFAULT NULL,
  `k` double DEFAULT NULL,
  `iCa` double DEFAULT NULL,
  `Li` double DEFAULT NULL,
  `pH` double DEFAULT NULL,
  `Cl` double DEFAULT NULL,
  `recordUUID` varchar(36) DEFAULT NULL,
  `date` datetime NOT NULL,
  `patientUUID` varchar(36) NOT NULL,
  PRIMARY KEY (`uuid`),
  KEY `el_pat_id_key` (`patientUUID`),
  KEY `el_rec_id_key` (`recordUUID`),
  CONSTRAINT `el_pat_id_fk` FOREIGN KEY (`patientUUID`) REFERENCES `patients` (`uuid`) ON DELETE CASCADE,
  CONSTRAINT `el_rec_id_fk` FOREIGN KEY (`recordUUID`) REFERENCES `examination_records` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `electrolytes`
--

LOCK TABLES `electrolytes` WRITE;
/*!40000 ALTER TABLE `electrolytes` DISABLE KEYS */;
INSERT INTO `electrolytes` VALUES ('a0d693e3-7cf0-11eb-9d49-f2cb56938ce8',4.5,14,3,4,5,3,'72b40eb2-9926-4475-8e97-4f44fc9a5aa0','2020-10-11 16:11:34','1e83137e-347e-4b8f-a414-66a7dc466064');
/*!40000 ALTER TABLE `electrolytes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `examination_records`
--

DROP TABLE IF EXISTS `examination_records`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `examination_records` (
  `uuid` varchar(36) NOT NULL,
  `date` datetime NOT NULL,
  `patientUUID` varchar(36) NOT NULL,
  PRIMARY KEY (`uuid`),
  KEY `exr_pat_id_key` (`patientUUID`),
  CONSTRAINT `exr_pat_id_fk` FOREIGN KEY (`patientUUID`) REFERENCES `patients` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `examination_records`
--

LOCK TABLES `examination_records` WRITE;
/*!40000 ALTER TABLE `examination_records` DISABLE KEYS */;
INSERT INTO `examination_records` VALUES ('72b40eb2-9926-4475-8e97-4f44fc9a5aa0','2020-10-11 16:11:34','1e83137e-347e-4b8f-a414-66a7dc466064');
/*!40000 ALTER TABLE `examination_records` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flyway_schema_history`
--

DROP TABLE IF EXISTS `flyway_schema_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flyway_schema_history` (
  `installed_rank` int(11) NOT NULL,
  `version` varchar(50) DEFAULT NULL,
  `description` varchar(200) NOT NULL,
  `type` varchar(20) NOT NULL,
  `script` varchar(1000) NOT NULL,
  `checksum` int(11) DEFAULT NULL,
  `installed_by` varchar(100) NOT NULL,
  `installed_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `execution_time` int(11) NOT NULL,
  `success` tinyint(1) NOT NULL,
  PRIMARY KEY (`installed_rank`),
  KEY `flyway_schema_history_s_idx` (`success`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flyway_schema_history`
--

LOCK TABLES `flyway_schema_history` WRITE;
/*!40000 ALTER TABLE `flyway_schema_history` DISABLE KEYS */;
INSERT INTO `flyway_schema_history` VALUES (1,'20201114011226','Init','SQL','V20201114011226__Init.sql',1268116104,'root','2021-03-04 13:50:48',92,1),(2,'20201114012121','Initial Table Structure','SQL','V20201114012121__Initial_Table_Structure.sql',-2105984112,'root','2021-03-04 13:50:49',1091,1),(3,'20210222230153','Change size varchar','SQL','V20210222230153__Change_size_varchar.sql',1339708758,'root','2021-03-04 13:50:50',92,1),(4,'20210224155003','Add heart rate type','SQL','V20210224155003__Add_heart_rate_type.sql',1511155784,'root','2021-03-04 13:50:50',31,1),(5,'20210301221251','Add new measurement types and requests','SQL','V20210301221251__Add_new_measurement_types_and_requests.sql',-2017260513,'root','2021-03-04 13:50:50',48,1),(6,'20210303111615','Add new usergroups','SQL','V20210303111615__Add_new_usergroups.sql',-484090383,'root','2021-03-04 13:50:50',24,1),(7,'20210304103312','add status to the nursing station','SQL','V20210304103312__add_status_to_the_nursing_station.sql',-2143366433,'root','2021-03-04 13:50:50',79,1);
/*!40000 ALTER TABLE `flyway_schema_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hematological_parameters`
--

DROP TABLE IF EXISTS `hematological_parameters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hematological_parameters` (
  `uuid` varchar(36) NOT NULL,
  `blood_glucose` double DEFAULT NULL,
  `hemoglobin` double DEFAULT NULL,
  `blood_gas` double DEFAULT NULL,
  `pH` double DEFAULT NULL,
  `pcO2` double DEFAULT NULL,
  `po2` double DEFAULT NULL,
  `hct` double DEFAULT NULL,
  `recordUUID` varchar(36) DEFAULT NULL,
  `date` datetime NOT NULL,
  `patientUUID` varchar(36) NOT NULL,
  PRIMARY KEY (`uuid`),
  KEY `he_pat_id_key` (`patientUUID`),
  KEY `he_rec_id_key` (`recordUUID`),
  CONSTRAINT `he_pat_id_fk` FOREIGN KEY (`patientUUID`) REFERENCES `patients` (`uuid`) ON DELETE CASCADE,
  CONSTRAINT `he_rec_id_fk` FOREIGN KEY (`recordUUID`) REFERENCES `examination_records` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hematological_parameters`
--

LOCK TABLES `hematological_parameters` WRITE;
/*!40000 ALTER TABLE `hematological_parameters` DISABLE KEYS */;
INSERT INTO `hematological_parameters` VALUES ('a0cea3c9-7cf0-11eb-9d49-f2cb56938ce8',4.5,14,3,4,5,3,3,'72b40eb2-9926-4475-8e97-4f44fc9a5aa0','2020-10-11 16:11:34','1e83137e-347e-4b8f-a414-66a7dc466064');
/*!40000 ALTER TABLE `hematological_parameters` ENABLE KEYS */;
UNLOCK TABLES;



--
-- Table structure for table `measurement_requests`
--

DROP TABLE IF EXISTS `measurement_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `measurement_requests` (
  `uuid` varchar(36) NOT NULL,
  `type` varchar(5) NOT NULL,
  `measurement_type` varchar(36) DEFAULT NULL,
  `patientUUID` varchar(36) NOT NULL,
  `deviceUUID` varchar(36) NOT NULL,
  `date` datetime NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`uuid`),
  KEY `mrq_type_key` (`measurement_type`),
  KEY `mrq_pat_id_key` (`patientUUID`),
  KEY `mrq_device_key` (`deviceUUID`),
  CONSTRAINT `mrq_device_fk` FOREIGN KEY (`deviceUUID`) REFERENCES `devices` (`uuid`) ON DELETE CASCADE,
  CONSTRAINT `mrq_pat_id_fk` FOREIGN KEY (`patientUUID`) REFERENCES `patients` (`uuid`) ON DELETE CASCADE,
  CONSTRAINT `mrq_type_fk` FOREIGN KEY (`measurement_type`) REFERENCES `measurement_types` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `measurement_requests`
--

LOCK TABLES `measurement_requests` WRITE;
/*!40000 ALTER TABLE `measurement_requests` DISABLE KEYS */;
/*!40000 ALTER TABLE `measurement_requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `measurement_types`
--

DROP TABLE IF EXISTS `measurement_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `measurement_types` (
  `uuid` varchar(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `mt_index` int(11) DEFAULT NULL,
  `classification` varchar(255) NOT NULL,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `measurement_types`
--

LOCK TABLES `measurement_types` WRITE;
/*!40000 ALTER TABLE `measurement_types` DISABLE KEYS */;
INSERT INTO `measurement_types` VALUES ('a09a0c12-7cf0-11eb-9d49-f2cb56938ce8','TEMPERATURE',1,'PHYSIOLOGICAL'),('a09a2110-7cf0-11eb-9d49-f2cb56938ce8','GLUCOSE',2,'HEMATOLOGICAL'),('a09a235d-7cf0-11eb-9d49-f2cb56938ce8','BLOOD_PRESSURE',3,'PHYSIOLOGICAL'),('a09a240e-7cf0-11eb-9d49-f2cb56938ce8','FLOW_RATE',4,'PHYSIOLOGICAL'),('a09a245d-7cf0-11eb-9d49-f2cb56938ce8','SATURATION',5,'PHYSIOLOGICAL'),('a1283cc7-7cf0-11eb-9d49-f2cb56938ce8','HEART_RATE',6,'PHYSIOLOGICAL'),('a13e5195-7cf0-11eb-9d49-f2cb56938ce8','TEMPERATURE_IR',7,'PHYSIOLOGICAL'),('a13e79a5-7cf0-11eb-9d49-f2cb56938ce8','HEMOGLOBIN',8,'HEMATOLOGICAL'),('a13e7a97-7cf0-11eb-9d49-f2cb56938ce8','ECG',9,'ECG'),('a13e7ad8-7cf0-11eb-9d49-f2cb56938ce8','BLOOD_GAS',10,'HEMATOLOGICAL');
/*!40000 ALTER TABLE `measurement_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `measurements_data`
--

DROP TABLE IF EXISTS `measurements_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `measurements_data` (
  `uuid` varchar(36) NOT NULL,
  `type` varchar(36) NOT NULL,
  `patientUUID` varchar(36) NOT NULL,
  `vale` double DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`uuid`),
  KEY `md_type_key` (`type`),
  KEY `md_pat_id_key` (`patientUUID`),
  CONSTRAINT `md_pat_id_fk` FOREIGN KEY (`patientUUID`) REFERENCES `patients` (`uuid`) ON DELETE CASCADE,
  CONSTRAINT `md_type_fk` FOREIGN KEY (`type`) REFERENCES `measurement_types` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `measurements_data`
--

LOCK TABLES `measurements_data` WRITE;
/*!40000 ALTER TABLE `measurements_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `measurements_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medication_prescriptions`
--

DROP TABLE IF EXISTS `medication_prescriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `medication_prescriptions` (
  `uuid` varchar(36) NOT NULL,
  `patientUUID` varchar(36) NOT NULL,
  `date` datetime NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `time` varchar(255) DEFAULT NULL,
  `duration` varchar(255) DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `visitUUID` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`uuid`),
  KEY `mpr_pat_id_key` (`patientUUID`),
  KEY `mpr_vst_id_key` (`visitUUID`),
  CONSTRAINT `mpr_pat_id_fk` FOREIGN KEY (`patientUUID`) REFERENCES `patients` (`uuid`) ON DELETE CASCADE,
  CONSTRAINT `mpr_vst_id_fk` FOREIGN KEY (`visitUUID`) REFERENCES `visits` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medication_prescriptions`
--

LOCK TABLES `medication_prescriptions` WRITE;
/*!40000 ALTER TABLE `medication_prescriptions` DISABLE KEYS */;
INSERT INTO `medication_prescriptions` VALUES ('a0f13bba-7cf0-11eb-9d49-f2cb56938ce8','1e83137e-347e-4b8f-a414-66a7dc466064','2020-11-03 22:58:18','Aerocort Inhaler','12:00','1 week','Levosalbutamol (50mcg) + Beclometasone (50mcg)','32612e5c-04a9-496d-89f2-fd3a15bf38cb');
/*!40000 ALTER TABLE `medication_prescriptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `metabolites`
--

DROP TABLE IF EXISTS `metabolites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `metabolites` (
  `uuid` varchar(36) NOT NULL,
  `glu` double DEFAULT NULL,
  `lac` double DEFAULT NULL,
  `recordUUID` varchar(36) DEFAULT NULL,
  `date` datetime NOT NULL,
  `patientUUID` varchar(36) NOT NULL,
  PRIMARY KEY (`uuid`),
  KEY `me_pat_id_key` (`patientUUID`),
  KEY `me_rec_id_key` (`recordUUID`),
  CONSTRAINT `me_pat_id_fk` FOREIGN KEY (`patientUUID`) REFERENCES `patients` (`uuid`) ON DELETE CASCADE,
  CONSTRAINT `me_rec_id_fk` FOREIGN KEY (`recordUUID`) REFERENCES `examination_records` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `metabolites`
--

LOCK TABLES `metabolites` WRITE;
/*!40000 ALTER TABLE `metabolites` DISABLE KEYS */;
INSERT INTO `metabolites` VALUES ('a0dac73c-7cf0-11eb-9d49-f2cb56938ce8',4.5,5,'72b40eb2-9926-4475-8e97-4f44fc9a5aa0','2020-10-11 16:11:34','1e83137e-347e-4b8f-a414-66a7dc466064');
/*!40000 ALTER TABLE `metabolites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `nursing_station_data`
--

DROP TABLE IF EXISTS `nursing_station_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nursing_station_data` (
  `uuid` varchar(36) NOT NULL,
  `patientUUID` varchar(36) NOT NULL,
  `deviceUUID` varchar(36) NOT NULL,
  `heart_rate` int(11) DEFAULT NULL,
  `o2_saturation` int(11) DEFAULT NULL,
  `blood_pressure_sys` int(11) DEFAULT NULL,
  `blood_pressure_dia` int(11) DEFAULT NULL,
  `datetime` datetime DEFAULT NULL,
  `past_tasks` varchar(255) DEFAULT NULL,
  `future_tasks` varchar(255) DEFAULT NULL,
  `pathology` varchar(255) DEFAULT NULL,
  `blood_glucose` double DEFAULT NULL,
  `temperature` double DEFAULT NULL,
  `drip_flow` tinyint(1) DEFAULT '0',
  `oxygen_flow` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`uuid`),
  KEY `ns_pat_id_key` (`patientUUID`),
  KEY `ns_dev_id_key` (`deviceUUID`),
  CONSTRAINT `ns_dev_id_fk` FOREIGN KEY (`deviceUUID`) REFERENCES `devices` (`uuid`) ON DELETE CASCADE,
  CONSTRAINT `ns_pat_id_fk` FOREIGN KEY (`patientUUID`) REFERENCES `patients` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `nursing_station_data`
--

LOCK TABLES `nursing_station_data` WRITE;
/*!40000 ALTER TABLE `nursing_station_data` DISABLE KEYS */;
INSERT INTO `nursing_station_data` VALUES ('a0e5d9f5-7cf0-11eb-9d49-f2cb56938ce8','1e83137e-347e-4b8f-a414-66a7dc466064','de542181-5960-4cea-b423-e5ed5decf541',60,92,120,70,'2020-11-03 22:58:18','Aerocort Inhaler-12:00','Aerocort Inhaler-18:00',NULL,4.5,37.3,1,13,NULL);
/*!40000 ALTER TABLE `nursing_station_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `observations`
--

DROP TABLE IF EXISTS `observations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `observations` (
  `uuid` varchar(36) NOT NULL,
  `taste` varchar(255) DEFAULT NULL,
  `smell` varchar(255) DEFAULT NULL,
  `consciousness` varchar(255) DEFAULT NULL,
  `recordUUID` varchar(36) DEFAULT NULL,
  `date` datetime NOT NULL,
  `patientUUID` varchar(36) NOT NULL,
  PRIMARY KEY (`uuid`),
  KEY `obs_pat_id_key` (`patientUUID`),
  KEY `obs_rec_id_key` (`recordUUID`),
  CONSTRAINT `obs_pat_id_fk` FOREIGN KEY (`patientUUID`) REFERENCES `patients` (`uuid`) ON DELETE CASCADE,
  CONSTRAINT `obs_rec_id_fk` FOREIGN KEY (`recordUUID`) REFERENCES `examination_records` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `observations`
--

LOCK TABLES `observations` WRITE;
/*!40000 ALTER TABLE `observations` DISABLE KEYS */;
INSERT INTO `observations` VALUES ('a0ca5585-7cf0-11eb-9d49-f2cb56938ce8','normal','normal','alert','72b40eb2-9926-4475-8e97-4f44fc9a5aa0','2020-10-11 16:11:34','1e83137e-347e-4b8f-a414-66a7dc466064');
/*!40000 ALTER TABLE `observations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pathology`
--

DROP TABLE IF EXISTS `pathology`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pathology` (
  `uuid` varchar(36) NOT NULL,
  `date` datetime(6) DEFAULT NULL,
  `data` longblob,
  `patientUUID` varchar(36) NOT NULL,
  PRIMARY KEY (`uuid`),
  KEY `pathology_pat_id_key` (`patientUUID`),
  CONSTRAINT `pathology_pat_id_fk` FOREIGN KEY (`patientUUID`) REFERENCES `patients` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pathology`
--

LOCK TABLES `pathology` WRITE;
/*!40000 ALTER TABLE `pathology` DISABLE KEYS */;
/*!40000 ALTER TABLE `pathology` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patient_family_details`
--

DROP TABLE IF EXISTS `patient_family_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patient_family_details` (
  `uuid` varchar(36) NOT NULL,
  `motherName` varchar(255) NOT NULL DEFAULT '',
  `motherStatus` tinyint(1) DEFAULT '0',
  `fatherName` varchar(255) NOT NULL DEFAULT '',
  `fatherStatus` tinyint(1) DEFAULT '0',
  `parentsTogether` tinyint(1) DEFAULT '0',
  `nextOfKin` varchar(255) DEFAULT NULL,
  `hereditaryDiseases` varchar(255) DEFAULT NULL,
  `nextOfKinRelationShip` varchar(255) DEFAULT NULL,
  `nextOfKinTelephone` varchar(255) DEFAULT NULL,
  `patientUUID` varchar(36) NOT NULL,
  PRIMARY KEY (`uuid`),
  KEY `pfd_pat_id_key` (`patientUUID`),
  CONSTRAINT `pfd_pat_id_fk` FOREIGN KEY (`patientUUID`) REFERENCES `patients` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patient_family_details`
--

LOCK TABLES `patient_family_details` WRITE;
/*!40000 ALTER TABLE `patient_family_details` DISABLE KEYS */;
INSERT INTO `patient_family_details` VALUES ('8b7aa058-3c5b-45f2-bc87-4532dfed5ddb','Emily Robertson',1,'Jack Robertson',1,1,'Max Robertson','None','brother','23452345','1e83137e-347e-4b8f-a414-66a7dc466064');
/*!40000 ALTER TABLE `patient_family_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patient_habits`
--

DROP TABLE IF EXISTS `patient_habits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patient_habits` (
  `uuid` varchar(36) NOT NULL,
  `habitName` varchar(255) NOT NULL,
  `habitYears` int(11) DEFAULT NULL,
  `habitFrequency` int(11) DEFAULT NULL,
  `patientUUID` varchar(36) NOT NULL,
  PRIMARY KEY (`uuid`),
  KEY `ph_pat_id_key` (`patientUUID`),
  CONSTRAINT `ph_pat_id_fk` FOREIGN KEY (`patientUUID`) REFERENCES `patients` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patient_habits`
--

LOCK TABLES `patient_habits` WRITE;
/*!40000 ALTER TABLE `patient_habits` DISABLE KEYS */;
/*!40000 ALTER TABLE `patient_habits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patient_health_cards`
--

DROP TABLE IF EXISTS `patient_health_cards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patient_health_cards` (
  `uuid` varchar(36) NOT NULL,
  `type` varchar(255) NOT NULL,
  `number` varchar(255) NOT NULL,
  `patientUUID` varchar(36) NOT NULL,
  PRIMARY KEY (`uuid`),
  KEY `phc_pat_id_key` (`patientUUID`),
  CONSTRAINT `phc_pat_id_fk` FOREIGN KEY (`patientUUID`) REFERENCES `patients` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patient_health_cards`
--

LOCK TABLES `patient_health_cards` WRITE;
/*!40000 ALTER TABLE `patient_health_cards` DISABLE KEYS */;
/*!40000 ALTER TABLE `patient_health_cards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patient_insurances`
--

DROP TABLE IF EXISTS `patient_insurances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patient_insurances` (
  `uuid` varchar(36) NOT NULL,
  `company` varchar(255) NOT NULL,
  `number` varchar(255) NOT NULL,
  `patientUUID` varchar(36) NOT NULL,
  PRIMARY KEY (`uuid`),
  KEY `pi_pat_id_key` (`patientUUID`),
  CONSTRAINT `pi_pat_id_fk` FOREIGN KEY (`patientUUID`) REFERENCES `patients` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patient_insurances`
--

LOCK TABLES `patient_insurances` WRITE;
/*!40000 ALTER TABLE `patient_insurances` DISABLE KEYS */;
/*!40000 ALTER TABLE `patient_insurances` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patient_medical_information`
--

DROP TABLE IF EXISTS `patient_medical_information`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patient_medical_information` (
  `uuid` varchar(36) NOT NULL,
  `bloodType` varchar(255) NOT NULL DEFAULT 'Unknown',
  `previousMedicalConditions` varchar(255) DEFAULT NULL,
  `natureOfMedicalCondition` varchar(255) DEFAULT NULL,
  `existingMedication` varchar(255) DEFAULT NULL,
  `allergies` varchar(255) DEFAULT NULL,
  `patientUUID` varchar(36) NOT NULL,
  PRIMARY KEY (`uuid`),
  KEY `pmf_pat_id_key` (`patientUUID`),
  CONSTRAINT `pmf_pat_id_fk` FOREIGN KEY (`patientUUID`) REFERENCES `patients` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patient_medical_information`
--

LOCK TABLES `patient_medical_information` WRITE;
/*!40000 ALTER TABLE `patient_medical_information` DISABLE KEYS */;
INSERT INTO `patient_medical_information` VALUES ('7dc31c67-adbe-488f-916d-ea5047e17ce3','A+','Good','Good','None','None','1e83137e-347e-4b8f-a414-66a7dc466064');
/*!40000 ALTER TABLE `patient_medical_information` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `patient_surgeries`
--

DROP TABLE IF EXISTS `patient_surgeries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `patient_surgeries` (
  `uuid` varchar(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  `year` varchar(255) NOT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `doctor` varchar(255) DEFAULT NULL,
  `patientUUID` varchar(36) NOT NULL,
  PRIMARY KEY (`uuid`),
  KEY `ps_pat_id_key` (`patientUUID`),
  CONSTRAINT `ps_pat_id_fk` FOREIGN KEY (`patientUUID`) REFERENCES `patients` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `patient_surgeries`
--

LOCK TABLES `patient_surgeries` WRITE;
/*!40000 ALTER TABLE `patient_surgeries` DISABLE KEYS */;
/*!40000 ALTER TABLE `patient_surgeries` ENABLE KEYS */;
UNLOCK TABLES;



--
-- Table structure for table `pharmacies`
--

DROP TABLE IF EXISTS `pharmacies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pharmacies` (
  `uuid` varchar(36) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pharmacies`
--

LOCK TABLES `pharmacies` WRITE;
/*!40000 ALTER TABLE `pharmacies` DISABLE KEYS */;
INSERT INTO `pharmacies` VALUES ('652a7a8a-3567-11eb-adc1-0242ac120002','centralpharmacy@pharm.com','Ave. 17','Bangalore','41023842342','Central Pharmacy'),('b1217a06-3567-11eb-adc1-0242ac120002','gandipharmacy@pharm.com','Ave. 17','Delhi','567345436','Gandhi Pharmacy'),('b6f2c4a8-3567-11eb-adc1-0242ac120002','sawhneypharmacy@pharm.com','Ave. 17','Delhi','34634780655','Sawhney Pharmacy');
/*!40000 ALTER TABLE `pharmacies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `photo_examinations`
--

DROP TABLE IF EXISTS `photo_examinations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `photo_examinations` (
  `uuid` varchar(36) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `type` varchar(36) NOT NULL,
  `photo` varchar(36) DEFAULT NULL,
  `recordUUID` varchar(36) DEFAULT NULL,
  `date` datetime NOT NULL,
  `patientUUID` varchar(36) NOT NULL,
  PRIMARY KEY (`uuid`),
  KEY `pe_pat_id_key` (`patientUUID`),
  KEY `pe_rec_id_key` (`recordUUID`),
  CONSTRAINT `pe_pat_id_fk` FOREIGN KEY (`patientUUID`) REFERENCES `patients` (`uuid`) ON DELETE CASCADE,
  CONSTRAINT `pe_rec_id_fk` FOREIGN KEY (`recordUUID`) REFERENCES `examination_records` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `photo_examinations`
--

LOCK TABLES `photo_examinations` WRITE;
/*!40000 ALTER TABLE `photo_examinations` DISABLE KEYS */;
/*!40000 ALTER TABLE `photo_examinations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `physical_measurements`
--

DROP TABLE IF EXISTS `physical_measurements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `physical_measurements` (
  `uuid` varchar(36) NOT NULL,
  `height` double DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `recordUUID` varchar(36) DEFAULT NULL,
  `date` datetime NOT NULL,
  `patientUUID` varchar(36) NOT NULL,
  PRIMARY KEY (`uuid`),
  KEY `pm_pat_id_key` (`patientUUID`),
  KEY `pm_rec_id_key` (`recordUUID`),
  CONSTRAINT `pm_pat_id_fk` FOREIGN KEY (`patientUUID`) REFERENCES `patients` (`uuid`) ON DELETE CASCADE,
  CONSTRAINT `pm_rec_id_fk` FOREIGN KEY (`recordUUID`) REFERENCES `examination_records` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `physical_measurements`
--

LOCK TABLES `physical_measurements` WRITE;
/*!40000 ALTER TABLE `physical_measurements` DISABLE KEYS */;
INSERT INTO `physical_measurements` VALUES ('a0c6b59b-7cf0-11eb-9d49-f2cb56938ce8',180,80,'72b40eb2-9926-4475-8e97-4f44fc9a5aa0','2020-10-11 16:11:34','1e83137e-347e-4b8f-a414-66a7dc466064');
/*!40000 ALTER TABLE `physical_measurements` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `physiological_parameters`
--

DROP TABLE IF EXISTS `physiological_parameters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `physiological_parameters` (
  `uuid` varchar(36) NOT NULL,
  `respiratory_rate` int(11) DEFAULT NULL,
  `o2_saturation` int(11) DEFAULT NULL,
  `o2_flow_rate` int(11) DEFAULT NULL,
  `blood_pressure_sys` int(11) DEFAULT NULL,
  `blood_pressure_dia` int(11) DEFAULT NULL,
  `heart_rate` int(11) DEFAULT NULL,
  `temperature` double DEFAULT NULL,
  `recordUUID` varchar(36) DEFAULT NULL,
  `date` datetime NOT NULL,
  `patientUUID` varchar(36) NOT NULL,
  PRIMARY KEY (`uuid`),
  KEY `pp_pat_id_key` (`patientUUID`),
  KEY `pp_rec_id_key` (`recordUUID`),
  CONSTRAINT `pp_pat_id_fk` FOREIGN KEY (`patientUUID`) REFERENCES `patients` (`uuid`) ON DELETE CASCADE,
  CONSTRAINT `pp_rec_id_fk` FOREIGN KEY (`recordUUID`) REFERENCES `examination_records` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `physiological_parameters`
--

LOCK TABLES `physiological_parameters` WRITE;
/*!40000 ALTER TABLE `physiological_parameters` DISABLE KEYS */;
INSERT INTO `physiological_parameters` VALUES ('a0c3561c-7cf0-11eb-9d49-f2cb56938ce8',65,96,5,120,75,55,36.6,'72b40eb2-9926-4475-8e97-4f44fc9a5aa0','2020-10-11 16:11:34','1e83137e-347e-4b8f-a414-66a7dc466064');
/*!40000 ALTER TABLE `physiological_parameters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usergroups`
--

DROP TABLE IF EXISTS `usergroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usergroups` (
  `uuid` varchar(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usergroups`
--

LOCK TABLES `usergroups` WRITE;
/*!40000 ALTER TABLE `usergroups` DISABLE KEYS */;
INSERT INTO `usergroups` VALUES ('6429b31b-d3bd-4482-af34-dd12534c6172','doctor','Doctor is responsible to check patients and test'),('67eded9b-26a1-4d7e-a7da-5dd9a08932a1','pathologist','Pathology is responsible to upload patients report'),('75e1e7cd-4bd0-4c53-8e06-6d929bb89881','radiologist','ok'),('8f8db272-61ea-4e7c-9b71-9a21bfd120a2','admin','User with all the privileges'),('981f7121-9238-451f-a1c9-a1d130b3c801','guest','Read Only Users'),('a1584125-7cf0-11eb-9d49-f2cb56938ce8','nurse','All Patients, Nursing Station'),('a1584615-7cf0-11eb-9d49-f2cb56938ce8','assistant_nurse','All Patients (BUT cannot add medications in visit page - hide that button), Nursing Station'),('a15846c9-7cf0-11eb-9d49-f2cb56938ce8','reception','All Patients (BUT cannot add medications in visit page - hide that button), Nursing Station'),('a158472d-7cf0-11eb-9d49-f2cb56938ce8','assistance','All Patients (BUT cannot add medications in visit page - hide that button), Nursing Station');
/*!40000 ALTER TABLE `usergroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `uuid` varchar(36) NOT NULL,
  `usergroup` varchar(36) NOT NULL,
  `username` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) DEFAULT NULL,
  `creationTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `email` varchar(255) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `birthDate` datetime DEFAULT NULL,
  `sex` varchar(4) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `aadhaarId` varchar(255) DEFAULT NULL,
  `education` varchar(255) DEFAULT NULL,
  `photoUUID` varchar(36) DEFAULT 'e4f4b0a6-2dc2-4e2e-873f-ee02cdf69ac7',
  `firstName` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `providerNumber` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `biometricUUID` varchar(36) DEFAULT NULL,
  `voIP_number` varchar(255) DEFAULT NULL,
  `hospitalUUID` varchar(36) NOT NULL,
  `deviceUUID` varchar(36) DEFAULT NULL,
  `prefferedPathologies` varchar(255) DEFAULT NULL,
  `prefferedPharmacies` varchar(255) DEFAULT NULL,
  `prefferedRadiologies` varchar(255) DEFAULT NULL,
  `centerId` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`uuid`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `user_aadhaar_uq` (`aadhaarId`),
  KEY `user_device_id_key` (`deviceUUID`),
  KEY `user_group_name_id_key` (`usergroup`),
  KEY `user_hospital_id_key` (`hospitalUUID`),
  CONSTRAINT `user_device_id_fk` FOREIGN KEY (`deviceUUID`) REFERENCES `devices` (`uuid`) ON DELETE CASCADE,
  CONSTRAINT `user_group_id_fk` FOREIGN KEY (`usergroup`) REFERENCES `usergroups` (`uuid`) ON DELETE CASCADE,
  CONSTRAINT `user_hospital_id_fk` FOREIGN KEY (`hospitalUUID`) REFERENCES `hospital` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('2bcf41d7-849e-47c2-9361-845bebc621d4','8f8db272-61ea-4e7c-9b71-9a21bfd120a2','admin@healthrecord.in','$2a$10$FI/PMO0oSHHosF2PX8l3QuB0DJepVfnynbLZ9Zm2711bF2ch8db2S','administrator',NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'e4f4b0a6-2dc2-4e2e-873f-ee02cdf69ac7','admin','admin','admin',NULL,NULL,NULL,'1111','59b254ff-7ba6-4c1a-82c8-356fe145dea6','de542181-5960-4cea-b423-e5ed5decf541',NULL,NULL,NULL,1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;



--
-- Table structure for table `vaccinations`
--

DROP TABLE IF EXISTS `vaccinations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vaccinations` (
  `uuid` varchar(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  `patientUUID` varchar(36) NOT NULL,
  PRIMARY KEY (`uuid`),
  KEY `pv_pat_id_key` (`patientUUID`),
  CONSTRAINT `pv_pat_id_fk` FOREIGN KEY (`patientUUID`) REFERENCES `patients` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vaccinations`
--

LOCK TABLES `vaccinations` WRITE;
/*!40000 ALTER TABLE `vaccinations` DISABLE KEYS */;
/*!40000 ALTER TABLE `vaccinations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `visits`
--

DROP TABLE IF EXISTS `visits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `visits` (
  `uuid` varchar(36) NOT NULL,
  `patientUUID` varchar(36) NOT NULL,
  `date` datetime NOT NULL,
  `note` varchar(255) DEFAULT NULL,
  `sms` tinyint(1) DEFAULT '0',
  `createdBy` varchar(255) DEFAULT NULL,
  `createdDate` datetime DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `type` varchar(4) DEFAULT '',
  `main_complaint_symptom` varchar(255) DEFAULT NULL,
  `main_complaint_diagnosis1` varchar(255) DEFAULT NULL,
  `main_complaint_diagnosis2` varchar(255) DEFAULT NULL,
  `main_complaint_diagnosis3` varchar(255) DEFAULT NULL,
  `secondary_complaint_diagnosis` varchar(255) DEFAULT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `examinations_prescribed` varchar(5000) DEFAULT NULL,
  `advisory` varchar(255) DEFAULT NULL,
  `next_visit_date` datetime DEFAULT NULL,
  `comments` varchar(255) DEFAULT NULL,
  `radiology_prescribed` varchar(5000) DEFAULT NULL,
  PRIMARY KEY (`uuid`),
  KEY `vst_pat_id_key` (`patientUUID`),
  CONSTRAINT `vst_pat_id_fk` FOREIGN KEY (`patientUUID`) REFERENCES `patients` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `visits`
--

LOCK TABLES `visits` WRITE;
/*!40000 ALTER TABLE `visits` DISABLE KEYS */;
INSERT INTO `visits` VALUES ('32612e5c-04a9-496d-89f2-fd3a15bf38cb','1e83137e-347e-4b8f-a414-66a7dc466064','2020-11-03 22:58:18',NULL,0,NULL,NULL,1,'H','Abdominal bloating,Ascites,Cardiomegaly','Albinism','Colo-rectal cancer',NULL,'','2020-11-03 22:58:18','17-Hydroxy Corticosteroids, Urine 24H ','Quit smoking','2021-05-04 00:00:00',NULL,'Angiography and interventional examination contrast material,Arteriography of heart chambers, coronary arteries');
/*!40000 ALTER TABLE `visits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calculated_parameters`
--

DROP TABLE IF EXISTS `calculated_parameters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calculated_parameters` (
  `uuid` varchar(36) NOT NULL,
  `Hb` double DEFAULT NULL,
  `hcO3` double DEFAULT NULL,
  `be` double DEFAULT NULL,
  `be_b` double DEFAULT NULL,
  `be_ecf` double DEFAULT NULL,
  `tcO2` double DEFAULT NULL,
  `ag` double DEFAULT NULL,
  `ag_k` double DEFAULT NULL,
  `O2sat` double DEFAULT NULL,
  `O2Ct` double DEFAULT NULL,
  `sbs` double DEFAULT NULL,
  `recordUUID` varchar(36) DEFAULT NULL,
  `date` datetime NOT NULL,
  `patientUUID` varchar(36) NOT NULL,
  PRIMARY KEY (`uuid`),
  KEY `cp_pat_id_key` (`patientUUID`),
  KEY `cp_rec_id_key` (`recordUUID`),
  CONSTRAINT `cp_pat_id_fk` FOREIGN KEY (`patientUUID`) REFERENCES `patients` (`uuid`) ON DELETE CASCADE,
  CONSTRAINT `cp_rec_id_fk` FOREIGN KEY (`recordUUID`) REFERENCES `examination_records` (`uuid`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calculated_parameters`
--

LOCK TABLES `calculated_parameters` WRITE;
/*!40000 ALTER TABLE `calculated_parameters` DISABLE KEYS */;
INSERT INTO `calculated_parameters` VALUES ('a0d233c8-7cf0-11eb-9d49-f2cb56938ce8',4.5,14,3,4,5,2,3,3,2,5,6,'72b40eb2-9926-4475-8e97-4f44fc9a5aa0','2020-10-11 16:11:34','1e83137e-347e-4b8f-a414-66a7dc466064');
/*!40000 ALTER TABLE `calculated_parameters` ENABLE KEYS */;
UNLOCK TABLES;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-05  1:18:09
