import os
from datetime import datetime
import logging
import time
import pandas

from mjconsort.schema import models

logging.basicConfig(level=logging.INFO)


def get_files():
    return [file.path for file in os.scandir('testdata')]


def yesto1(w):
    if w == 'Yes':
        return 1
    return 0


def p_age(w):
    return w.split('years')[0].strip()


def tstamp_tdate(w):
    return w.date().__str__()


known_usergroup = (lambda val: '8f8db272-61ea-4e7c-9b71-9a21bfd120a2')


def tstamp(t):
    return datetime.utcnow()
def tstamp2(t):
    return None

class Demo:
    def __init__(self):
        self.files = get_files()
    def do(self):
        for file in self.files:
            logging.info(f'Reading a spreadsheet with name {file}!')

            # hospital
            healthdata = pandas.ExcelFile(file)
            hospital_df_ = pandas.read_excel(healthdata, 'hospitals').fillna('')
            hospital_df = hospital_df_.rename(index=str, columns={'htps://': 'websiteAddress'})
            known_hospital = hospital_df.to_dict(orient='records')[0]
            try:
                hospital.insert(known_hospital).execute()
                logging.info(f'HOSPITAL  {known_hospital["name"]} SAVED for {file}')
            except Exception as error:
                logging.info(f'HOSPITAL, {error}')

            # devices
            healthdata = pandas.ExcelFile(file)
            devices_df = pandas.read_excel(healthdata, 'devices').fillna('')
            devices_df['uuid'] = devices_df['machineId']
            known_device = devices_df.to_dict(orient='records')[0]
            try:
                devices.insert(known_device).execute()
                logging.info(f'DEVICE with  {d["uuid"]} SAVED for {file}')
            except Exception as error:
                logging.info(f'DEVICES, {error}')

            # usergroups
            # Universal usergroups, use the same values everywhere.

            known_device_fn = (lambda val: known_device['uuid'])
            known_hospital_fn = (lambda val: known_hospital['uuid'])
            known_user_fn = (lambda val: known_user['uuid'])
            
	    # user
            logging.info('USER TABLE: updating')
            healthdata = pandas.ExcelFile(file)
            user_df = pandas.read_excel(healthdata, 'user').fillna('')
            user_df['birthDate'] = user_df.birthDate.apply(tstamp_tdate)
            user_df['usergroup'] = user_df.usergroup.apply(known_usergroup)
            user_df['deviceUUID'] = user_df.deviceUUID.apply(known_device_fn)
            user_df['creationTime'] = user_df.creationTime.apply(tstamp2) 
            known_user = user_df.to_dict(orient='records')[0]
            try:
                user.insert(known_user).execute()
                logging.info(f'USER  {known_user["uuid"]} SAVED for {file}')
            except Exception as error:
                logging.info(f'USER, {error}') 
            # return 
            # patient

            healthdata = pandas.ExcelFile(file)
            patient_df = pandas.read_excel(healthdata, 'patients').fillna('')
            patient_df['birthDate'] = patient_df.birthDate.apply(tstamp_tdate)
            
            patient_df['deviceUUID'] = patient_df.deviceUUID.apply(known_device_fn)          
            patient_df['hospitalUUID'] = patient_df.hospitalUUID.apply(known_hospital_fn)
            patient_df['creationTime'] = patient_df.creationTime.apply(tstamp2) 
            patient_df['createdByUserUUID'] = patient_df.createdByUserUUID.apply(known_user_fn)
            patient_list = patient_df.to_dict(orient='records')
            for p in patient_list:
                try:
                    patients.insert(p).execute()
                    logging.info(f'PATIENT  {p["name"]} SAVED for {file}')
                except Exception as error:
                    logging.info(error)



if __name__ == "__main__":
    logging.info('Running test data, please stand by!')
    Demo().do()
